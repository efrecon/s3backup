#!/usr/bin/env sh

ROOT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
LIBDIR=
[ -d "${ROOT_DIR}/lib" ] && LIBDIR="${ROOT_DIR}/lib"
[ -z "$LIBDIR" ] && [ -d "${ROOT_DIR}/../lib" ] && LIBDIR="${ROOT_DIR}/../lib"
[ -z "$LIBDIR" ] && echo "Cannot find lib dir!" >&2 && exit 1

set -ef

# All (good?) defaults
S3_VERBOSE=${S3_VERBOSE:-0}
S3_TRACE=${S3_TRACE:-0}

TAR_DESTINATION=${TAR_DESTINATION:-.}
TAR_SOURCE=${TAR_SOURCE:-}
TAR_PREFIX=${TAR_PREFIX:-}
TAR_PATHS=${TAR_PATHS:-}
# default program to generate sums, can also be sha1sum, md5sum, etc.
TAR_SUM=${TAR_SUM:-sha256sum}
TAR_YOUNGEST=${TAR_YOUNGEST:-0}
TAR_SINGLE=${TAR_SINGLE:-"-"}
TAR_INPLACE=${TAR_INPLACE:-0}
TAR_PREPROCESSOR=${TAR_PREPROCESSOR:-}
TAR_PP_EXT=${TAR_PP_EXT:-}
TAR_SIG_EXT=${TAR_SIG_EXT:-".sig"}
TAR_SIG_STAT_OPT=${TAR_SIG_STAT_OPT:-"%N %s %Y %A %U:%G"}

# Dynamic vars
cmdname=$(basename "$0")
appname=${cmdname%.*}

if [ "$appname" = "untar" ]; then
    TAR_UNTAR=${TAR_UNTAR:-1}
else
    TAR_UNTAR=${TAR_UNTAR:-0}
fi

# Will be set and hold files when tar operation and not in place.
TAR_TMPDIR=

# Print usage on stderr and exit
usage() {
  exitcode="$1"
  cat << USAGE >&2

Description:

  $cmdname will tar files/dirs from a source directory to a destination dir.
  The destination directory will contain date-based tar files. Execute the
  command formed after all options (preferably) separated from options via
  a double-dash upon success

Usage:

  $cmdname [-option arg --long-option(=)arg] [--] command

  where all dash-led options are as follows (long options can be followed by
  an equal sign):
    -v | --verbose      Be more verbose
    --trace             Trace all underlying calls to mc before they happen
    -d | --destination  Path to destination directory (defaults to pwd)
    -s | --source       Path to source directory
    -p | --prefix       Prefix to add to destination tar file names
    -f | --files        (list of) files/dirs patterns to pack and compress in
                        tar file
    -u | --untar        Pick latest from source directory and unpack to
                        destination. This is the behaviour when the program
                        is called untar (without ext.)
    --tar               Force tar behaviour (as opposed to untar)
    --single            Keep only one version of each tar file, use dir as
                        signature storage between runs. Disable totally with -,
                        e.g. multiple allowed. Empty dir for no storage.

  In addition, $cmdname recognises a number of mc options, these are forwarded
  as is, whenever relevant. Consult the mc documentation at https://docs.min.io/
  for more information. Forwarded options are:
    -q | --quiet      disable progress bar display
    --insecure        disable SSL certificate verification
    --debug           enable debug output
    --no-color        disable color theme
    -C | --config-dir path to configuration folder (default: "$HOME/.mc")
    --encrypt-key     encrypt/decrypt objects (using server-side encryption with
                      customer provided keys)

USAGE
  exit "$exitcode"
}

module() {
    for module in "$@"; do
        module_path="${LIBDIR}/${module}.sh"
        if [ -f "$module_path" ]; then
            # shellcheck disable=SC1090
            . "$module_path"
        else
            echo "Cannot find module $module at $module_path !" >& 2
            exit 1
        fi
    done
}

# Source in all relevant modules. This is where most of the "stuff" will occur.
module log s3
module "s3_$S3"


# Parse options
while [ $# -gt 0 ]; do
    case "$1" in
        -d | --dst | --dest | --destination)
            TAR_DESTINATION=$2; shift 2;;
        --dst=* | --dest=* | --destination=*)
            TAR_DESTINATION="${1#*=}"; shift 1;;

        -s | --src | --source)
            TAR_SOURCE=$2; shift 2;;
        --src=* | --source=*)
            TAR_SOURCE="${1#*=}"; shift 1;;

        -f | --files)
            TAR_PATHS=$2; shift 2;;
        --files=*)
            TAR_PATHS="${1#*=}"; shift 1;;

        -p | --prefix)
            TAR_PREFIX=$2; shift 2;;
        --prefix=*)
            TAR_PREFIX="${1#*=}"; shift 1;;

        -c | --check | --sum | --checksum)
            TAR_SUM=$2; shift 2;;
        --check=* | --sum=* | --checksum=*)
            TAR_SUM="${1#*=}"; shift 1;;

        -u | --untar)
            TAR_UNTAR=1; shift;;

        --tar)
            TAR_UNTAR=0; shift;;

        --youngest)
            TAR_YOUNGEST=1; shift;;

        --single)
            TAR_SINGLE=$2; shift 2;;
        --single=*)
            TAR_SINGLE="${1#*=}"; shift 1;;

        --in-place | --inplace)
            TAR_INPLACE=1; shift;;

        --preprocess | --preprocessor)
            TAR_PREPROCESSOR=$2; shift 2;;
        --preprocess=* | --preprocessor=*)
            TAR_PREPROCESSOR="${1#*=}"; shift 1;;

        --ext | --extension)
            TAR_PP_EXT=$2; shift 2;;
        --ext=* | --extension=*)
            TAR_PP_EXT="${1#*=}"; shift 1;;

        -C | --config-dir)
            s3_options --config-dir "$2"; shift 2;;
        --config-dir=*)
            s3_options --config-dir "${1#*=}"; shift 1;;

        -q | --quiet | --debug | --insecure | --no-color)
            s3_options "$1"; shift;;

        --encrypt-key)
            s3_encrypt "$2"; shift 2;;
        --encrypt-key=*)
            s3_encrypt "${1#*=}"; shift 1;;

        -v | --verbose)
            # shellcheck disable=SC2034
            S3_VERBOSE=1; shift;;

        --trace)
            # shellcheck disable=SC2034
            S3_TRACE=1; shift;;

        -h | --help)
            usage 0;;
        --)
            shift; break;;
        -*)
            echo "Unknown option: $1 !" >&2 ; usage 1;;
        *)
            break;;
    esac
done


latest() {
    log "Looking for latest valid file starting with $2 in $1"

    if [ -z "$TAR_PP_EXT" ]; then
        ptn="${1%/}/${2}*.tgz"
    else
        ptn="${1%/}/${2}*.tgz.${TAR_PP_EXT#.*}"
    fi
    s3_ls "$ptn" | while IFS= read -r latest; do
        if [ -n "$latest" ]; then
            if [ -n "$TAR_SUM" ]; then
                # Generate an extension from the sum generating program by
                # removing the word "sum" from its name and guess the file
                ext=$(basename "$TAR_SUM" | sed 's/sum//g')
                if [ -z "$TAR_PP_EXT" ]; then
                    sumfile="$(printf %s\\n "$latest" | sed 's/\.tgz$//i').${ext}"
                else
                    sumfile="$(printf %s\\n "$latest" | sed "s/\.tgz\.${TAR_PP_EXT#.*}\$//i").${ext}"
                fi
                # When we are requested to use checksums, there need to be a
                # checksum file and its content need to match the current
                # checksum of the file so we will be able to take it into
                # account.
                if s3_exists "$sumfile"; then
                    log "Checking $ext checksum of $latest using $sumfile"
                    chksum=$(s3_cat "$sumfile")
                    nowsum=$(s3_cat "$latest" | $TAR_SUM | awk '{print $1}')
                    if [ "$nowsum" = "$chksum" ]; then
                        log "Found latest valid file matching $(basename "$ptn") at $latest"
                        printf %s\\n "$latest"
                        break
                    else
                        if [ "$TAR_YOUNGEST" = "1" ]; then
                            warn "Checksum mismatch for $latest, aborting!!"
                            break
                        else
                            warn "Checksum mismatch for $latest, ignoring!!"
                        fi
                    fi
                else
                    log "No checksum for $latest, skipping"
                fi
            else
                log "Found latest file matching $(basename "$ptn") at $latest"
                printf %s\\n "$latest"
                break
            fi
        fi
    done
}


# Expand the patterns in TAR_PATHS into their physical dirs/files on
# disk, under TAR_SOURCE. Return a space separated list
expand() (
    if [ -n "$TAR_SOURCE" ]; then
        cd "$TAR_SOURCE"
    fi

    find . -maxdepth 1 -mindepth 1 | while IFS= read -r path; do
        for ptn in $TAR_PATHS; do
            #shellcheck disable=SC2254 # Expansion on purpose!
            case "$(basename "$path")" in
                $ptn)
                    printf "%s " "$(basename "$path")";;
            esac
        done
    done
)


run_tar() {
    tarfile=$1

    if [ -n "$TAR_SOURCE" ]; then
        set -- -C "$TAR_SOURCE"
    fi

    if [ "$S3_VERBOSE" = "1" ]; then
        set -- "$@" -czvf
    else
        set -- "$@" -czf
    fi

    if [ -z "$TAR_PREPROCESSOR" ]; then
        # shellcheck disable=SC2086 # No quotes on purpose to glob
        tar "$@" "$tarfile" $TAR_PATHS
    else
        # shellcheck disable=SC2086 # No quotes on purpose to glob
        tar "$@" - $TAR_PATHS | $TAR_PREPROCESSOR > "$tarfile"
    fi
}


signature() {
    log "Computing signature of $1"
    if [ -d "$1" ]; then
        find "$1" -type f -exec stat -c "$TAR_SIG_STAT_OPT" '{}' \;
    else
        stat -c "$TAR_SIG_STAT_OPT" "$1"
    fi
}

# Returns false when change was detected
detect_change() {
    change=1
    for path in $TAR_PATHS; do
        sig_path="$(printf %s\\n "$path" | sha256sum | cut -f 1 -d " ")${TAR_SIG_EXT}"
        if s3_exists "${1%%/}/$sig_path"; then
            now_sig=$(mktemp)
            signature "${TAR_SOURCE:-"."}/$path" > "$now_sig"
            # Following only works if source is local
            if s3_cat "${1%%/}/$sig_path" | diff -q "$now_sig" - >/dev/null; then
                log "No change in $path"
            else
                s3_cp "$now_sig" "${1%%/}/$sig_path"
                rm -f "$now_sig"
                log "Updated signature for $path at ${1%%/}/$sig_path"
                change=0
                break
            fi
        else
            signature "${TAR_SOURCE:-"."}/$path" > "${1%%/}/$sig_path"
            log "Stored signature for $path at ${1%%/}/$sig_path"
            change=0
        fi
    done

    return $change
}

op_tar() {
    log "*** $appname *** tar-compressing files $TAR_PATHS in $TAR_SOURCE into $TAR_DESTINATION"

    TAR_PATHS=$(expand)

    log "Creating destination directory at $TAR_DESTINATION"
    s3_mkdir "${TAR_DESTINATION%%/}/"

    TYPE=$(s3_detect "${TAR_DESTINATION%%/}/")
    [ "$TYPE" = "s3" ] && [ "$TAR_INPLACE" = "1" ] && abort "You cannot tar in place to s3 buckets!"
    if [ "$TAR_SINGLE" = "-" ]; then
        prev=""
    elif [ -z "$TAR_SINGLE" ]; then
        log "Finding previous tar at $TAR_DESTINATION"
        prev=$(latest "$TAR_DESTINATION" "$TAR_PREFIX")
    else
        log "Creating signature cache directory at ${TAR_DESTINATION%%/}/${TAR_SINGLE}"
        s3_mkdir "${TAR_DESTINATION%%/}/${TAR_SINGLE%%/}/"

        # When no change was detected, just return
        if ! detect_change "${TAR_DESTINATION%%/}/${TAR_SINGLE%%/}"; then
            log "No change since last run"
            return
        fi
    fi

    # Decide destination. Most of the time, we use a temporary directory.
    now=$(date +%Y%m%d%H%M%S)
    if [ "$TAR_INPLACE" = "1" ]; then
        dstdir=$TAR_DESTINATION
    else
        TAR_TMPDIR=$(mktemp -d)
        dstdir=$TAR_TMPDIR
    fi

    # Decide name of tar file, adding preprocessor extension if relevant.
    if [ -z "$TAR_PP_EXT" ]; then
        tarfile="${dstdir}/${TAR_PREFIX}${now}.tgz"
    else
        tarfile="${dstdir}/${TAR_PREFIX}${now}.tgz.${TAR_PP_EXT#.*}"
    fi

    # Perform tar operation, preprocessing data if necessary.
    log "Archiving and compressing $TAR_PATHS from $TAR_SOURCE to $tarfile"
    run_tar "$tarfile"

    # Run sha256sum (or similar) on the generated file, if relevant.
    if [ -n "$TAR_SUM" ]; then
        # Generate an extension from the sum generating program by removing
        # the word "sum" from its name
        ext=$(basename "$TAR_SUM" | sed 's/sum//g')
        chksum=$("$TAR_SUM" "$tarfile" | awk '{print $1}')
        if [ -n "$chksum" ]; then
            sumfile="${dstdir}/${TAR_PREFIX}${now}.${ext}"
            log "Storing $ext checksum $chksum at $sumfile"
            printf %s\\n "$chksum" > "$sumfile"
        else
            warn "Cannot generate checksum for $tarfile with $TAR_SUM!!"
        fi
    fi

    # Remove newly created tar (and sum) if it is similar to the previous
    # one, when prev is non-empty, i.e. when we should not keep multiple
    # copies of the same tar. Note: you'd better give a directory for
    # storing signatures between runs as it will avoid generating tar files
    # and sha256 sums before diffing.
    if [ -n "$prev" ]; then
        # Do a (quick!) diff on sum files when they are available, otherwise
        # we have to diff on file content.
        if [ -n "$TAR_SUM" ]; then
            ext=$(basename "$TAR_SUM" | sed 's/sum//g')
            if [ -z "$TAR_PP_EXT" ]; then
                prevsumfile="$(printf %s\\n "$prev" | sed 's/\.tgz$//i').${ext}"
            else
                prevsumfile="$(printf %s\\n "$prev" | sed "s/\.tgz\.${TAR_PP_EXT#.*}\$//i").${ext}"
            fi
            prevsum=$(s3_cat "$prevsumfile")
            if [ "$prevsum" = "$chksum" ]; then
                log "Previous file at $prev identical, removing new"
                rm -f "$tarfile"
                rm -f "$sumfile"
            else
                log "Previous file at $prev differs, checksum was $prevsum"
                if [ "$TAR_INPLACE" = "1" ] && [ "$S3_VERBOSE" = "1" ]; then
                    echo "$tarfile"
                    echo "$sumfile"
                fi
            fi
        else
            if s3_diff "$prev" "$tarfile"; then
                log "Previous file at $prev identical, removing new"
                rm -f "$tarfile"
            else
                log "Previous file at $prev differs"
                if [ "$TAR_INPLACE" = "1" ] && [ "$S3_VERBOSE" = "1" ]; then
                    echo "$tarfile"
                fi
            fi
        fi

    fi
    if [ "$TAR_INPLACE" = "0" ] && [ -f "$tarfile" ]; then
        log "Copying tar file $tarfile to destination ${TAR_DESTINATION}"
        s3_cp "$tarfile" "${TAR_DESTINATION%%/}/$(basename "$tarfile")"
        [ "$S3_VERBOSE" = "1" ] && echo "${TAR_DESTINATION}/$(basename "$tarfile")"
        if [ -n "$TAR_SUM" ] && [ -f "$sumfile" ]; then
            log "Copying sum file $sumfile to destination ${TAR_DESTINATION}"
            s3_cp "$sumfile" "${TAR_DESTINATION%%/}/$(basename "$sumfile")"
            [ "$S3_VERBOSE" = "1" ] && echo "${TAR_DESTINATION}/$(basename "$sumfile")"
        fi
    fi
    [ "$TAR_INPLACE" = "0" ] && rm -rf "$dstdir";  # Remove temporary directory
}

op_untar() {
    log "*** $appname *** restoring files $TAR_PATHS from latest tar-compressed backup at $TAR_SOURCE into $TAR_DESTINATION"

    latest=$(latest "$TAR_SOURCE" "$TAR_PREFIX")
    if [ -n "$latest" ]; then
        log "Unpacking content of $latest to $TAR_DESTINATION"
        # shellcheck disable=SC2086
        if [ "$S3_VERBOSE" = "1" ]; then
            if [ -z "$TAR_PREPROCESSOR" ]; then
                s3_cat "$latest" | tar -C "$TAR_DESTINATION" -zxvf - $TAR_PATHS
            else
                s3_cat "$latest" | $TAR_PREPROCESSOR | tar -C "$TAR_DESTINATION" -zxvf - $TAR_PATHS
            fi
        else
            if [ -z "$TAR_PREPROCESSOR" ]; then
                s3_cat "$latest" | tar -C "$TAR_DESTINATION" -zxf - $TAR_PATHS
            else
                s3_cat "$latest" | $TAR_PREPROCESSOR | tar -C "$TAR_DESTINATION" -zxf - $TAR_PATHS
            fi
        fi
    fi
}

# shellcheck disable=SC2120 # Arg passed from trap
cleanup() {
    SIGNAL=${1:-}
    if [ -n "$SIGNAL" ]; then
        log "Caught $SIGNAL! Cleaning up..."
        trap - "$SIGNAL" # clear the trap
    fi
    # Remove temporary directory and its content
    if [ -n "${TAR_TMPDIR:-}" ] && [ -d "$TAR_TMPDIR" ]; then
        rm -rf "$TAR_TMPDIR"
        log "Cleaned up $TAR_TMPDIR"
    fi
    # Cleanup implementation specific files, if any
    s3_cleanup
}


[ -z "$TAR_SOURCE" ] && abort "You need to specify a source!"
[ -z "$TAR_DESTINATION" ] && abort "You need to specify a destination"
[ -z "${TAR_PATHS}" ] && [ "$TAR_UNTAR" = "0" ] && abort "You need to specify sources to tar and compress with --files"
[ -z "$TAR_PREPROCESSOR" ] && [ -n "$TAR_PP_EXT" ] && abort "You cannot specify a preprocessor extension without a preprocessor!"

s3_init
[ "$(s3_type "$TAR_SOURCE")" != "folder" ] && abort "$TAR_SOURCE must be a directory"

TAR_DESTINATION=${TAR_DESTINATION%%+(/)}
TAR_SOURCE=${TAR_SOURCE%%+(/)}

if [ "$TAR_UNTAR" = "0" ]; then
    op_tar
else
    op_untar
fi

trap "cleanup INT" INT
trap "cleanup TERM" TERM

# On success, cleanup and execute command passed as argument
cleanup
if [ $# -ne "0" ]; then
    log "Executing $*"
    exec "$@"
fi