#!/usr/bin/env sh

ROOT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
LIBDIR=
[ -d "${ROOT_DIR}/lib" ] && LIBDIR="${ROOT_DIR}/lib"
[ -z "$LIBDIR" ] && [ -d "${ROOT_DIR}/../lib" ] && LIBDIR="${ROOT_DIR}/../lib"
[ -z "$LIBDIR" ] && echo "Cannot find lib dir!" >&2 && exit 1

set -ef

# All (good?) defaults
S3_VERBOSE=${S3_VERBOSE:-0}
S3_TRACE=${S3_TRACE:-0}

IFCOPY_SOURCE=${IFCOPY_SOURCE:-}
IFCOPY_DESTINATION=${IFCOPY_DESTINATION:-}
IFCOPY_FORCE=${IFCOPY_FORCE:-0}
IFCOPY_SIBLINGS=${IFCOPY_SIBLINGS:-}
IFCOPY_PATTERN=${IFCOPY_PATTERN:-"*"}
IFCOPY_INPLACE=${IFCOPY_INPLACE:-0}
IFCOPY_PREPROCESSOR=${IFCOPY_PREPROCESSOR:-}
IFCOPY_PP_EXT=${IFCOPY_PP_EXT:-}
IFCOPY_RENAME=${IFCOPY_RENAME:-}

# Dynamic vars
cmdname=$(basename "$(readlink -f "$0")")
appname=${cmdname%.*}

# Print usage on stderr and exit
usage() {
  exitcode="$1"
  cat << USAGE >&2

Description:

  $cmdname will look if the latest file found in a source directory is
  different from the previous file and will copy it to a destination
  S3 bucket if they are different. A command can be executed once done,
  preferrably separated from the rest of the options by a double-dash

Usage:
  $cmdname [-option arg --long-option(=)arg] [--] command

  where all dash-led options are as follows (long options can be followed by
  an equal sign):
    -v | --verbose   Be more verbose
    --trace          Trace all underlying calls to mc before they happen
    -s | --source    Path to files to look for differences
    -d | --dest      Path to directory to copy to
    -p | --pattern   (glob)pattern to match subset of files in source
    -n | --rename    Name of destination files, dynamic tokens will be replaced
    --siblings       List of file extensions to copy along main file (eg. .sha)
    --force          Force copy even if files have identical content

  In addition, $cmdname recognises a number of mc options, these are forwarded
  as is, whenever relevant. Consult the mc documentation at https://docs.min.io/
  for more information. Forwarded options are:
    -q | --quiet      disable progress bar display
    --insecure        disable SSL certificate verification
    --debug           enable debug output
    --no-color        disable color theme
    -C | --config-dir path to configuration folder (default: "$HOME/.mc")
    --encrypt-key     encrypt/decrypt objects (using server-side encryption with
                      customer provided keys)

  When renaming, the following tokens will be dynamically recognised and
  replaced using the name of the original file (or its sibling). In addition,
  the template can use date formatting tokens, e.g. %Y or %H. The timestamp
  used will be the time at which all copies are made, including siblings. This
  means that date-related tokens will all have the same value across all copies.
  %basename%   basename of the file
  %noext%      basename of the file, without any extension
  %ext%        extension of the basename of the file
  %extension%  same as %ext%

USAGE
  exit "$exitcode"
}


module() {
    for module in "$@"; do
        module_path="${LIBDIR}/${module}.sh"
        if [ -f "$module_path" ]; then
            # shellcheck disable=SC1090
            . "$module_path"
        else
            echo "Cannot find module $module at $module_path !" >& 2
            exit 1
        fi
    done
}

# Source in all relevant modules. This is where most of the "stuff" will occur.
module log s3
module "s3_$S3"

# Parse options
while [ $# -gt 0 ]; do
    case "$1" in
        -s | --src | --source)
            IFCOPY_SOURCE=$2; shift 2;;
        --src=* | --source=*)
            IFCOPY_SOURCE="${1#*=}"; shift 1;;

        -d | --dst | --dest | --destination)
            IFCOPY_DESTINATION=$2; shift 2;;
        --dst=* | --dest=* | --destination=*)
            IFCOPY_DESTINATION="${1#*=}"; shift 1;;

        -p | --pattern)
            IFCOPY_PATTERN=$2; shift 2;;
        --pattern=*)
            IFCOPY_PATTERN="${1#*=}"; shift 1;;

        --siblings)
            IFCOPY_SIBLINGS=$2; shift 2;;
        --siblings=*)
            IFCOPY_SIBLINGS="${1#*=}"; shift 1;;

        --force)
            IFCOPY_FORCE=1; shift;;

        --in-place | --inplace)
            IFCOPY_INPLACE=1; shift;;

        --preprocess | --preprocessor)
            IFCOPY_PREPROCESSOR=$2; shift 2;;
        --preprocess=* | --preprocessor=*)
            IFCOPY_PREPROCESSOR="${1#*=}"; shift 1;;

        --ext | --extension)
            IFCOPY_PP_EXT=$2; shift 2;;
        --ext=* | --extension=*)
            IFCOPY_PP_EXT="${1#*=}"; shift 1;;

        -n | --rename)
            IFCOPY_RENAME=$2; shift 2;;
        --rename=*)
            IFCOPY_RENAME="${1#*=}"; shift 1;;

        -C | --config-dir)
            s3_options --config-dir "$2"; shift 2;;
        --config-dir=*)
            s3_options --config-dir "${1#*=}"; shift 1;;

        -q | --quiet | --debug | --insecure | --no-color)
            s3_options "$1"; shift;;

        --encrypt-key)
            s3_encrypt "$2"; shift 2;;
        --encrypt-key=*)
            s3_encrypt "${1#*=}"; shift 1;;

        --trace)
            # shellcheck disable=SC2034
            S3_TRACE=1; shift;;

        -v | --verbose)
            # shellcheck disable=SC2034
            S3_VERBOSE=1; shift;;

        -h | --help)
            usage 0;;
        --)
            shift; break;;
        -*)
            echo "Unknown option: $1 !" >&2 ; usage 1;;
        *)
            break;;
    esac
done

tokenise() {
    _bname=$(basename "$1")
    if [ -z "$IFCOPY_RENAME" ]; then
        printf %s\\n "$_bname"
    else
        _tmpname=$( printf %s\\n "$IFCOPY_RENAME" |
                    sed -E \
                            -e "s/%basename%/$_bname/g" \
                            -e "s/%noext%/${_bname%%.*}/g" \
                            -e "s/%ext%/${_bname##*.}/g" \
                            -e "s/%extension%/${_bname##*.}/g" )
        if [ "$#" -gt "1" ]; then
            date +"$_tmpname"
        else
            date -d "@$2" +"$_tmpname"
        fi
        unset _tmpname
    fi
    unset _bname
}

do_copy() {
    _now=$(date +%s)
    if [ -z "$IFCOPY_PREPROCESSOR" ]; then
        d_fname="${IFCOPY_DESTINATION}/$(tokenise "$1" "$_now")"
        log "Copying $1 as $d_fname"
        s3_cp "$1" "$d_fname"
        [ "$S3_VERBOSE" = "1" ] && echo "$d_fname"
    else
        if [ -z "$IFCOPY_PP_EXT" ]; then
            d_fname="${IFCOPY_DESTINATION}/$(tokenise "$1" "$_now")"
        else
            d_fname="${IFCOPY_DESTINATION}/$(tokenise "$1" "$_now").${IFCOPY_PP_EXT#.*}"
        fi

        log "Preprocessing $1 and copying as $d_fname"
        # pipe doesn't seem to work all the time, so going through a temporary
        # file, which we dislike a bit. It seems to be connected to running
        # minio in gateway mode. This is still as secure as it can be: we
        # typically have a copy of the original unencrypted file at hand on the
        # disk anyway!
        if [ "$IFCOPY_INPLACE" = "1" ]; then
            $IFCOPY_PREPROCESSOR < "$1" | s3_mc pipe "$d_fname"
        else
            tmp_fname=$(mktemp)
            $IFCOPY_PREPROCESSOR < "$1" > "$tmp_fname"
            s3_cp "$tmp_fname" "$d_fname"
            rm -f "$tmp_fname"
        fi
        [ "$S3_VERBOSE" = "1" ] && echo "$d_fname"
    fi

    if [ -n "$IFCOPY_SIBLINGS" ]; then
        for ext in $IFCOPY_SIBLINGS; do
            sibling=$(printf %s\\n "$1" | sed -E "s/\.[[:alnum:]]*$/\.${ext}/i")
            if s3_exists "$sibling"; then
                s_fname=${IFCOPY_DESTINATION}/$(tokenise "$sibling" "$_now")
                log "Copying $sibling, sibling of $1 as $s_fname"
                s3_cp "$sibling" "$s_fname"
                [ "$S3_VERBOSE" = "1" ] && echo "$s_fname"
            fi
        done
    fi
}


# Sanity check on arguments
[ -z "$IFCOPY_SOURCE" ] && abort "You need to specify sources!"
[ -z "$IFCOPY_DESTINATION" ] && abort "You need to specify a destination"
[ -n "$IFCOPY_SIBLINGS" ] && [ "$IFCOPY_PATTERN" = '*' ] && abort "List of siblings is incompatible with match-all pattern"
[ -z "$IFCOPY_PREPROCESSOR" ] && [ -n "$IFCOPY_PP_EXT" ] && abort "You cannot specify a preprocessor extension without a preprocessor!"

s3_init

[ "$(s3_type "$IFCOPY_SOURCE")" != "folder" ] && abort "$IFCOPY_SOURCE must be a directory"
log "Creating destination directory at $IFCOPY_DESTINATION"
s3_mkdir "${IFCOPY_DESTINATION}/"

# Universally remove trailing slashes so we can assume there are not, now that
# we know that the source and destination were actually folders.
IFCOPY_SOURCE=${IFCOPY_SOURCE%%+(/)}
IFCOPY_DESTINATION=${IFCOPY_DESTINATION%%+(/)}

log "*** $appname *** Looking for latest differences in $IFCOPY_SOURCE and siblings $IFCOPY_SIBLINGS and copying to $IFCOPY_DESTINATION on changes"

if [ -z "$IFCOPY_PP_EXT" ]; then
    D_PTN="${IFCOPY_PATTERN}"
else
    D_PTN="${IFCOPY_PATTERN}.${IFCOPY_PP_EXT#.*}"
fi
log "Counting files matching $IFCOPY_PATTERN in $IFCOPY_SOURCE"
NB_FILES=$(s3_ls "${IFCOPY_SOURCE}/${IFCOPY_PATTERN}" | wc -l)
if [ "$NB_FILES" = "0" ]; then
    log "No files yet at $IFCOPY_SOURCE"
elif [ "$NB_FILES" = "1" ]; then
    FNAME=$(s3_ls "${IFCOPY_SOURCE}/${IFCOPY_PATTERN}" | head -n 1)
    log "First file available as $FNAME, copying to $IFCOPY_DESTINATION"
    do_copy "$FNAME"
elif [ "$NB_FILES" -ge "2" ]; then
    LATEST=$(s3_ls "${IFCOPY_SOURCE}/${IFCOPY_PATTERN}" | head -n 1)
    PREVIOUS=$(s3_ls "${IFCOPY_SOURCE}/${IFCOPY_PATTERN}" | head -n 2 | tail -n 1)
    if [ "$IFCOPY_FORCE" = "1" ] || ! s3_diff "$LATEST" "$PREVIOUS"; then
        log "$LATEST different from $PREVIOUS, copying to $IFCOPY_DESTINATION"
        do_copy "$LATEST"
    elif [ "$(s3_ls "${IFCOPY_DESTINATION}/${D_PTN}" | wc -l)" = "0" ]; then
        log "No file at $IFCOPY_DESTINATION yet, copying latest"
        do_copy "$LATEST"
    else
        log "$LATEST and $PREVIOUS identical, no copy!"
    fi
fi

s3_cleanup

if [ $# -ne "0" ]; then
    log "Executing $*"
    exec "$@"
fi
