ARG TARGETARCH=amd64

# Pick the proper python version, compatibility match version of s3cmd.
# But also released versions of the python image at the Docker library.
ARG PYTHON_VERSION=3.11
ARG ALPINE_VERSION=3.20
FROM --platform=linux/$TARGETARCH python:${PYTHON_VERSION}-alpine${ALPINE_VERSION}

# Minio release. behaviour and arguments have changed between versions, so
# double check compatibility.
ARG MINIO_VERSION=RELEASE.2024-06-05T18-13-30Z
ARG TARGETARCH

ADD https://dl.minio.io/client/mc/release/linux-${TARGETARCH}/archive/mc.${MINIO_VERSION} /usr/bin/mc
RUN chmod a+x /usr/bin/mc

# Set age version
ARG AGE_VERSION=1.1.1
# Install age, so it can be used for client-side file encryption when the remote
# S3 does not support encryption, e.g. when running in (some?) gateway mode.
RUN wget -q -O /tmp/age.tgz https://github.com/FiloSottile/age/releases/download/v${AGE_VERSION}/age-v${AGE_VERSION}-"$(uname -s | tr '[:upper:]' '[:lower:]')"-${TARGETARCH}.tar.gz && \
    tar x -C /tmp -vf /tmp/age.tgz && \
    mv -f /tmp/age/age* /usr/local/bin && \
    rm -rf /tmp/age*

# Version to use for s3cmd, this is the one which the wrapper was tested
# against.
ARG S3CMD_VERSION=2.4.0
# Install s3cmd
RUN wget -q -O /tmp/s3cmd.tgz https://github.com/s3tools/s3cmd/releases/download/v${S3CMD_VERSION}/s3cmd-${S3CMD_VERSION}.tar.gz && \
    tar x -C /tmp -zvf /tmp/s3cmd.tgz && \
    cd /tmp/s3cmd-${S3CMD_VERSION} && \
    apk add --no-cache libmagic && \
    pip install --break-system-packages python-magic && \
    python3 setup.py install && \
    cd / && \
    rm -rf /tmp/s3cmd*

COPY lib/*.sh /usr/local/lib/
COPY *.sh /usr/local/bin/

ENTRYPOINT [ "ash" ]
