#!/usr/bin/env sh

# Shell sanity
set -eu

# Root directory of the script
SCRIPT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
ROOT_DIR=$(dirname "$SCRIPT_DIR")

# Tests to perform.
TESTS=${TESTS:-"tar ifcopy cleaner"}

# Underlying implementation to use for tests.
IMPLEMENTATION=${IMPLEMENTATION:-mc}

# Keep stuff for debugging problems
KEEP=${KEEP:-0}

# Dynamic vars
cmdname=$(basename "$(readlink -f "$0")")
# shellcheck disable=SC2034
appname=${cmdname%.*}

# shellcheck disable=SC2034
S3_VERBOSE=1
# shellcheck disable=SC1091
. "${ROOT_DIR}/lib/log.sh"

IMAGE=
CONTAINER=

rnd() {
    size=${1:-8}
    chunk=$((size*32))
    dd if=/dev/urandom bs=1 count="$chunk" 2>/dev/null |
        tr -dc "${2:-"a-z0-9"}" |
        head -c "$size"
}

prepare() {
    if [ -z "$IMAGE" ]; then
        IMAGE=s3backup-test-$(rnd)
        log "Preparing image $IMAGE"
        if ! docker build -t "$IMAGE" "$ROOT_DIR"; then
            IMAGE=
        fi
    fi

    if [ -z "$CONTAINER" ] && [ -n "$IMAGE" ]; then
        CONTAINER=$(docker run \
                            -d \
                            --entrypoint=ash \
                            -e "S3=$IMPLEMENTATION" \
                            -e "S3_TRACE=1" \
                            -v "$ROOT_DIR:/s3backup" \
                            -v "${SCRIPT_DIR}/config:/root/.s3cmd:ro" \
                        "$IMAGE" \
                            -c "while true ; do sleep 1 ; done")
    fi
    if [ -z "$CONTAINER" ]; then
        abort "Could not create container for tests!"
    fi
}

BUCKET_NAME=com.gitlab.efrecon.s3backup.$(rnd)
BUCKET_PATH=play/$BUCKET_NAME

dexe() {
    if [ -z "$CONTAINER" ]; then
        prepare
    fi

    if [ -z "$CONTAINER" ]; then
        abort "No Docker container to run tests in!"
    else
        log "Running $* in container $CONTAINER"
        docker exec -it "$CONTAINER" "$@"
    fi
}


exit_script() {
    SIGNAL=${1:-}
    [ -n "$SIGNAL" ] && warn "Caught $SIGNAL! Cleaning up..."

    if [ "$KEEP" = "0" ]; then
        log "Removing temporary bucket $BUCKET_PATH"
        case "$IMPLEMENTATION" in
            mc) dexe mc rb --force --dangerous "$BUCKET_PATH";;
            s3cmd) dexe s3cmd --config=/s3backup/tests/config/play.cfg --force --recursive rb "s3://$BUCKET_NAME/";;
        esac
    fi
    [ -n "$CONTAINER" ] && docker rm -fv "$CONTAINER"
    [ -n "$IMAGE" ] && docker image rm "$IMAGE"
    trap - "$SIGNAL" # clear the trap
    exit $?
}

trap "exit_script INT" INT
trap "exit_script TERM" TERM
trap "exit_script EXIT" EXIT

log "Creating temporary bucket $BUCKET_PATH"
case "$IMPLEMENTATION" in
    mc) dexe mc mb "$BUCKET_PATH";;
    s3cmd) dexe s3cmd --config=/s3backup/tests/config/play.cfg mb "s3://$BUCKET_NAME";;
esac

if printf %s\\n "$TESTS" | grep -q "tar"; then
    log "$(purple "*T*E*S*T*") -- Running tar.sh once without sum"
    dexe /s3backup/tar.sh \
        --verbose \
        --source /s3backup \
        --destination "${BUCKET_PATH}/tar/clear" \
        --sum= \
        --files "lib tests"
    log "$(purple "*T*E*S*T*") -- Running tar.sh without sum a second time, with --single option to avoid pushing again"
    dexe /s3backup/tar.sh \
        --verbose \
        --source /s3backup \
        --destination "${BUCKET_PATH}/tar/clear" \
        --files "lib tests" \
        --sum= \
        --single
    log "$(purple "*T*E*S*T*") -- Running tar.sh once with sum"
    dexe /s3backup/tar.sh \
        --verbose \
        --source /s3backup \
        --destination "${BUCKET_PATH}/tar/sum" \
        --files "lib tests"
    log "$(purple "*T*E*S*T*") -- Running tar.sh with sum a second time, with --single option to avoid pushing again"
    dexe /s3backup/tar.sh \
        --verbose \
        --source /s3backup \
        --destination "${BUCKET_PATH}/tar/sum" \
        --files "lib tests" \
        --single
    log "$(purple "*T*E*S*T*") -- Running tar.sh once with preprocessor"
    dexe /s3backup/tar.sh \
        --verbose \
        --source /s3backup \
        --destination "${BUCKET_PATH}/tar/ppc" \
        --sum= \
        --preprocessor cat \
        --extension .ppc \
        --files "lib tests"
    log "$(purple "*T*E*S*T*") -- Running tar.sh with preprocessor a second time, with --single option to avoid pushing again"
    dexe /s3backup/tar.sh \
        --verbose \
        --source /s3backup \
        --destination "${BUCKET_PATH}/tar/ppc" \
        --sum= \
        --preprocessor cat \
        --extension .ppc \
        --files "lib tests" \
        --single
    log "$(purple "*T*E*S*T*") -- Running tar.sh once with preprocessor and sum"
    dexe /s3backup/tar.sh \
        --verbose \
        --source /s3backup \
        --destination "${BUCKET_PATH}/tar/ppc-sum" \
        --preprocessor cat \
        --extension .ppc \
        --files "lib tests"
    log "$(purple "*T*E*S*T*") -- Running tar.sh with preprocessor and sum a second time, with --single option to avoid pushing again"
    dexe /s3backup/tar.sh \
        --verbose \
        --source /s3backup \
        --destination "${BUCKET_PATH}/tar/ppc-sum" \
        --preprocessor cat \
        --extension .ppc \
        --files "lib tests" \
        --single
    log "$(purple "*T*E*S*T*") -- Running tar.sh with encryption"
    tarfile=$(  dexe /s3backup/tar.sh \
                    --verbose \
                    --source /s3backup \
                    --destination "${BUCKET_PATH}/tar/encrypted" \
                    --files "lib tests" \
                    --single \
                    --encrypt-key "${BUCKET_PATH}/tar/encrypted=$(rnd 43)" |
                head -n 1)
    log "Check encrypted content is garbage"
    case "$IMPLEMENTATION" in
        mc)
            log "mc cannot check"
            ;;
        s3cmd)
            s3_path=$(printf %s \\n "$tarfile" | sed -E -e 's~^play/~s3://~')
            if dexe s3cmd --config=/s3backup/tests/config/play.cfg --quiet get "$s3_path" - | tar ztvf - >/dev/null 2>&1; then
                warn "Content should not have been visible!"
            fi
            ;;
    esac
    stty sane
fi

file_cat() {
    case "$IMPLEMENTATION" in
        mc)
            dexe mc cat "$1";;
        s3cmd)
            s3_path=$(printf %s\\n "$1" | sed -E -e 's~^play/~s3://~')
            dexe s3cmd --config=/s3backup/tests/config/play.cfg get "$s3_path" -
            ;;
    esac
}

dir_content() {
    case "$IMPLEMENTATION" in
        mc)
            dexe mc ls "$1";;
        s3cmd)
            s3_path=$(printf %s\\n "$1" | sed -E -e 's~^play/~s3://~')
            dexe s3cmd --config=/s3backup/tests/config/play.cfg ls "${s3_path%%/}/" | grep -v '.s3empty$'
            ;;
    esac
}

file_count() {
    dir_content "$1" | wc -l
}

file_copy() {
    case "$IMPLEMENTATION" in
        mc)
            dexe mc cp "$1" "$2";;
        s3cmd)
            s3_path=$(printf %s\\n "$2" | sed -E -e 's~^play/~s3://~')
            dexe s3cmd --config=/s3backup/tests/config/play.cfg put "$1" "$s3_path"
            ;;
    esac
}

if printf %s\\n "$TESTS" | grep -q "ifcopy"; then
    log "Preparing directory in destination"
    tmpdir=/tmp/$(rnd 12)
    dexe mkdir -p "$tmpdir"
    dexe cp /s3backup/tests/test.sh "${tmpdir}/"

    log "$(purple "*T*E*S*T*") -- Copying latest main script to remote"
    dexe /s3backup/ifcopy.sh \
        --verbose \
        --source "$tmpdir" \
        --destination "${BUCKET_PATH}/ifcopy/clear" \
        --pattern '*.sh'
    if ! dir_content "${BUCKET_PATH}/ifcopy/clear" | grep -q test.sh; then
        warn "Failed copying"
        dexe ls "$tmpdir"
    fi
    log "$(purple "*T*E*S*T*") -- Checking no copy happens when nothing has changed"
    dexe cp /s3backup/tests/test.sh "${tmpdir}/test_copy.sh"
    dexe /s3backup/ifcopy.sh \
        --verbose \
        --source "$tmpdir" \
        --destination "${BUCKET_PATH}/ifcopy/clear" \
        --pattern '*.sh'
    if dir_content "${BUCKET_PATH}/ifcopy/clear" | grep -q test_copy.sh; then
        warn "Should not have copied again!"
        dexe ls "$tmpdir"
    fi

    # Now remove the copy so we can trigger again on "first file==push"
    dexe rm "${tmpdir}/test_copy.sh"

    log "$(purple "*T*E*S*T*") -- Copying latest main script to remote through a preprocessor"
    dexe /s3backup/ifcopy.sh \
        --verbose \
        --source "$tmpdir" \
        --destination "${BUCKET_PATH}/ifcopy/preprocess" \
        --preprocessor cat \
        --extension .ppc \
        --pattern '*.sh'
    if ! dir_content "${BUCKET_PATH}/ifcopy/preprocess" | grep -q test.sh.ppc; then
        warn "Failed copying"
        dexe ls "$tmpdir"
    fi

    log "$(purple "*T*E*S*T*") -- Copying latest main script, renamed to remote"
    dexe /s3backup/ifcopy.sh \
        --verbose \
        --source "$tmpdir" \
        --destination "${BUCKET_PATH}/ifcopy/renamed" \
        --rename "%noext%-%Y%m%d-%H%M%S.%ext%" \
        --pattern '*.sh'
    if ! dir_content "${BUCKET_PATH}/ifcopy/renamed" | grep -qE 'test-[0-9]{4}[0-9]{2}[0-9]{2}-[0-9]{2}[0-9]{2}[0-9]{2}.sh'; then
        warn "Failed copying"
        dexe ls "$tmpdir"
    fi

    log "Cleaning directory in destination"
    dexe rm -rf "$tmpdir"
fi

if printf %s\\n "$TESTS" | grep -q "cleaner"; then
    log "Preparing directory in destination"
    tmpdir=/tmp/$(rnd 12)
    dexe mkdir -p "$tmpdir"

    # Copy some files now.
    for i in $(seq 5); do
        file_copy /s3backup/tests/test.sh "${BUCKET_PATH}/cleaner/test_${i}.sh"
    done

    log "$(purple "*T*E*S*T*") -- Keep all"
    dexe /s3backup/cleaner.sh \
        --verbose \
        --keep 0 \
        --directory "${BUCKET_PATH}/cleaner" \
        --pattern '*.sh'
    if [ "$(file_count "${BUCKET_PATH}/cleaner")" != "5" ]; then
        warn "Failed cleaning"
    fi

    log "$(purple "*T*E*S*T*") -- Keep 5 keeps all"
    dexe /s3backup/cleaner.sh \
        --verbose \
        --keep 5 \
        --directory "${BUCKET_PATH}/cleaner" \
        --pattern '*.sh'
    if [ "$(file_count "${BUCKET_PATH}/cleaner")" != "5" ]; then
        warn "Failed cleaning"
    fi

    log "$(purple "*T*E*S*T*") -- Remmove all"
    dexe /s3backup/cleaner.sh \
        --verbose \
        --age 0s \
        --directory "${BUCKET_PATH}/cleaner" \
        --pattern '*.sh'
    if [ -n "$(file_count "${BUCKET_PATH}/cleaner")" ]; then
        warn "Failed cleaning"
    fi

    # Copy some files again
    for i in $(seq 5); do
        file_copy /s3backup/tests/test.sh "${BUCKET_PATH}/cleaner/test_${i}.sh"
    done

    log "$(purple "*T*E*S*T*") -- Keep 4 keeps 4, edge case close to 5"
    dexe /s3backup/cleaner.sh \
        --verbose \
        --keep 4 \
        --directory "${BUCKET_PATH}/cleaner" \
        --pattern '*.sh'
    if [ "$(file_count "${BUCKET_PATH}/cleaner")" != "4" ]; then
        warn "Failed cleaning"
    fi

    log "$(purple "*T*E*S*T*") -- Keep 2"
    dexe /s3backup/cleaner.sh \
        --verbose \
        --keep 2 \
        --directory "${BUCKET_PATH}/cleaner" \
        --pattern '*.sh'
    if [ "$(file_count "${BUCKET_PATH}/cleaner")" != "2" ]; then
        warn "Failed cleaning"
    fi

fi