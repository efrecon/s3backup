#!/usr/bin/env sh

ROOT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
LIBDIR=
[ -d "${ROOT_DIR}/lib" ] && LIBDIR="${ROOT_DIR}/lib"
[ -z "$LIBDIR" ] && [ -d "${ROOT_DIR}/../lib" ] && LIBDIR="${ROOT_DIR}/../lib"
[ -z "$LIBDIR" ] && echo "Cannot find lib dir!" >&2 && exit 1

# shell sanity
set -eu

# All (good?) defaults
S3_VERBOSE=${S3_VERBOSE:-0}
S3_TRACE=${S3_TRACE:-0}

IFENV_PRESENT=${IFENV_PRESENT:-}
IFENV_EMPTY=${IFENV_EMPTY:-}
IFENV_NONEMPTY=${IFENV_NONEMPTY:-}

# Dynamic vars
cmdname=$(basename "$(readlink -f "$0")")
appname=${cmdname%.*}

# Print usage on stderr and exit
usage() {
  exitcode="$1"
  cat << USAGE >&2

Description:

  $cmdname will test for presence, emptiness or value of environment
  variables and continue with a command once all tests have passed.

Usage:
  $cmdname [-option arg --long-option(=)arg] [--] command

  where all dash-led options are as follows (long options can be followed by
  an equal sign):
    -p | --presence  Space separated list of variable names (default: none)
    -z | --empty     Space separated list of empty var names (default: none)
    -n | --nonempty  Space separated list of valued var names (default: none)
    -v | --verbose   Be more verbose
    --trace          Be even more verbose
    -h | --help      Print this help and exit.

  The remaining command will be executed in place if, and only if, all tests
  have passed.

USAGE
  exit "$exitcode"
}

module() {
    for module in "$@"; do
        module_path="${LIBDIR}/${module}.sh"
        if [ -f "$module_path" ]; then
            # shellcheck disable=SC1090
            . "$module_path"
        else
            echo "Cannot find module $module at $module_path !" >& 2
            exit 1
        fi
    done
}

# Source in all relevant modules. This is where most of the "stuff" will occur.
module log

# Parse options
while [ $# -gt 0 ]; do
    case "$1" in
        -p | --present)
            IFENV_PRESENT=$2; shift 2;;
        --present=*)
            IFENV_PRESENT="${1#*=}"; shift 1;;

        -z | --empty)
            IFENV_EMPTY=$2; shift 2;;
        --empty=*)
            IFENV_EMPTY="${1#*=}"; shift 1;;

        -n | --nonempty)
            IFENV_NONEMPTY=$2; shift 2;;
        --nonempty=*)
            IFENV_NONEMPTY="${1#*=}"; shift 1;;

        --trace)
            # shellcheck disable=SC2034
            S3_TRACE=1; shift;;

        -v | --verbose)
            # shellcheck disable=SC2034
            S3_VERBOSE=1; shift;;

        -h | --help)
            usage 0;;
        --)
            shift; break;;
        -*)
            echo "Unknown option: $1 !" >&2 ; usage 1;;
        *)
            break;;
    esac
done


log "*** $appname *** Starting to test for environment variables"

# Check presence of variables
if [ -n "$IFENV_PRESENT" ]; then
  for v in $IFENV_PRESENT; do
    trace "Testing presence of $v"
    if env | grep -q "^${v}="; then
      log "$v was present"
    else
      abort "$v was not present"
    fi
  done
fi

# Check emptiness of specified variables. Non-existing variables will be
# considered empty. Combine with -p if necessary.
if [ -n "$IFENV_EMPTY" ] ; then
  for v in $IFENV_EMPTY; do
    trace "Testing emptiness or absence of $v"
    if env | grep -q "^${v}="; then
      if env | grep -qE "^${v}=\$"; then
        log "$v was present but empty"
      else
        abort "$v was not empty"
      fi
    else
      log "$v was not present, deemed empty"
    fi
  done
fi

# Check variables for a value
if [ -n "$IFENV_NONEMPTY" ] ; then
  for v in $IFENV_NONEMPTY; do
    trace "Testing non emptiness of $v"
    if env | grep -qE "^${v}=.+\$"; then
      log "$v was not empty"
    else
      abort "$v was empty"
    fi
  done
fi

# Once here, we have performed all the checks successfully, go on executing what
# was specified at the command line after the (optional) --
if [ $# -ne "0" ]; then
    log "Executing $*"
    exec "$@"
fi
