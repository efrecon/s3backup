#!/usr/bin/env sh

ROOT_DIR=$( cd -P -- "$(dirname -- "$(command -v -- "$0")")" && pwd -P )
LIBDIR=
[ -d "${ROOT_DIR}/lib" ] && LIBDIR="${ROOT_DIR}/lib"
[ -z "$LIBDIR" ] && [ -d "${ROOT_DIR}/../lib" ] && LIBDIR="${ROOT_DIR}/../lib"
[ -z "$LIBDIR" ] && echo "Cannot find lib dir!" >&2 && exit 1

set -ef

# All (good?) defaults
S3_VERBOSE=${S3_VERBOSE:-0}
S3_TRACE=${S3_TRACE:-0}

CLEANER_KEEP=${CLEANER_KEEP:-0}
CLEANER_AGE=${CLEANER_AGE:-}
CLEANER_DRYRUN=${CLEANER_DRYRUN:-0}
CLEANER_SIBLINGS=${CLEANER_SIBLINGS:-}
CLEANER_PATTERN=${CLEANER_PATTERN:-"*"}
CLEANER_TARGET=${CLEANER_TARGET:-}

# Dynamic vars
cmdname=$(basename "$(readlink -f $0)")
appname=${cmdname%.*}

# Print usage on stderr and exit
usage() {
  exitcode="$1"
  cat << USAGE >&2

Description:

  $cmdname will find the latest files matching a pattern and only keep a
  finite amount of the youngest files

Usage:
  $cmdname [-option arg --long-option(=)arg] ([--] command)

  where all dash-led options are as follows (long options can be followed by
  an equal sign):
    -d | --dir(ectory) Directory to remove files from
    -p | --pattern     (glob)pattern to match subset of files in directory
    -k | --keep        Number of files to keep, defaults to 0, meaning keep all
                       and do nothing
    -a | --age         Files older than this period will be removed, defaults to
                       empty, meaning keep all and do nothing
    --siblings         List of file extensions to copy along main file (eg.
                       .sha256 or .md5)
    --dry(-)run        Do not remove, just show (likely to enter an infinite
                       loop by construction!)
    -v | --verbose     Be more verbose
    --trace            Trace all underlying calls to mc before they happen

  In addition, $cmdname recognises a number of mc options, these are forwarded
  as is, whenever relevant. Consult the mc documentation at https://docs.min.io/
  for more information. Forwarded options are:
    -q | --quiet      disable progress bar display
    --insecure        disable SSL certificate verification
    --debug           enable debug output
    --no-color        disable color theme
    -C | --config-dir path to configuration folder (default: "$HOME/.mc")
    --encrypt-key     encrypt/decrypt objects (using server-side encryption with
                      customer provided keys)

USAGE
  exit "$exitcode"
}

module() {
    for module in "$@"; do
        module_path="${LIBDIR}/${module}.sh"
        if [ -f "$module_path" ]; then
            # shellcheck disable=SC1090
            . "$module_path"
        else
            echo "Cannot find module $module at $module_path !" >& 2
            exit 1
        fi
    done
}

# Source in all relevant modules. This is where most of the "stuff" will occur.
module log s3
module "s3_$S3"

# Parse options
while [ $# -gt 0 ]; do
    case "$1" in
        -k | --keep)
            CLEANER_KEEP=$2; shift 2;;
        --keep=*)
            CLEANER_KEEP="${1#*=}"; shift 1;;

        -a | --age)
            CLEANER_AGE=$2; shift 2;;
        --age=*)
            CLEANER_AGE="${1#*=}"; shift 1;;

        -d | --dir | --directory)
            CLEANER_TARGET=$2; shift 2;;
        --dir=* | --directory=*)
            CLEANER_TARGET="${1#*=}"; shift 1;;

        -p | --pattern)
            CLEANER_PATTERN=$2; shift 2;;
        --pattern=*)
            CLEANER_PATTERN="${1#*=}"; shift 1;;

        --siblings)
            CLEANER_SIBLINGS=$2; shift 2;;
        --siblings=*)
            CLEANER_SIBLINGS="${1#*=}"; shift 1;;

        --dry-run | --dryrun)
            CLEANER_DRYRUN=1; shift;;

        -C | --config-dir)
            s3_options --config-dir "$2"; shift 2;;
        --config-dir=*)
            s3_options --config-dir "${1#*=}"; shift 1;;

        -q | --quiet | --debug | --insecure | --no-color)
            s3_options "$1"; shift;;

        --encrypt-key)
            s3_encrypt "$2"; shift 2;;
        --encrypt-key=*)
            s3_encrypt "${1#*=}"; shift 1;;

        --trace)
            # shellcheck disable=SC2034
            S3_TRACE=1; shift;;

        -v | --verbose)
            # shellcheck disable=SC2034
            S3_VERBOSE=1; shift;;

        -h | --help)
            usage 0;;
        --)
            shift; break;;
        -*)
            echo "Unknown option: $1 !" >&2 ; usage 1;;
        *)
            break;;
    esac
done

# Return the approx. number of seconds for the human-readable period passed as a
# parameter
howlong() {
    if echo "$1"|grep -Eqo '^[0-9]+[[:space:]]*[yY]'; then
        len=$(echo "$1"  | sed -En 's/([0-9]+)[[:space:]]*[yY].*/\1/p')
        expr "$len" \* 31536000
        return
    fi
    if echo "$1"|grep -Eqo '^[0-9]+[[:space:]]*[Mm][Oo]'; then
        len=$(echo "$1"  | sed -En 's/([0-9]+)[[:space:]]*[Mm][Oo].*/\1/p')
        expr "$len" \* 2592000
        return
    fi
    if echo "$1"|grep -Eqo '^[0-9]+[[:space:]]*[Mm][Ii]'; then
        len=$(echo "$1"  | sed -En 's/([0-9]+)[[:space:]]*[Mm][Ii].*/\1/p')
        expr "$len" \* 60
        return
    fi
    if echo "$1"|grep -Eqo '^[0-9]+[[:space:]]*m'; then
        len=$(echo "$1"  | sed -En 's/([0-9]+)[[:space:]]*m.*/\1/p')
        expr "$len" \* 2592000
        return
    fi
    if echo "$1"|grep -Eqo '^[0-9]+[[:space:]]*[Ww]'; then
        len=$(echo "$1"  | sed -En 's/([0-9]+)[[:space:]]*[Ww].*/\1/p')
        expr "$len" \* 604800
        return
    fi
    if echo "$1"|grep -Eqo '^[0-9]+[[:space:]]*[Dd]'; then
        len=$(echo "$1"  | sed -En 's/([0-9]+)[[:space:]]*[Dd].*/\1/p')
        expr "$len" \* 86400
        return
    fi
    if echo "$1"|grep -Eqo '^[0-9]+[[:space:]]*[Hh]'; then
        len=$(echo "$1"  | sed -En 's/([0-9]+)[[:space:]]*[Hh].*/\1/p')
        expr "$len" \* 3600
        return
    fi
    if echo "$1"|grep -Eqo '^[0-9]+[[:space:]]*M'; then
        len=$(echo "$1"  | sed -En 's/([0-9]+)[[:space:]]*M.*/\1/p')
        expr "$len" \* 60
        return
    fi
    if echo "$1"|grep -Eqo '^[0-9]+[[:space:]]*[Ss]'; then
        len=$(echo "$1"  | sed -En 's/([0-9]+)[[:space:]]*[Ss].*/\1/p')
        echo "$len"
        return
    fi
    if echo "$1"|grep -Eqo '^[0-9]+'; then
        echo "$1"
        return
    fi
}

no_extension() {
    if printf %s\\n "$1" | grep -q '/'; then
        _dir=$(dirname "$1")
        _fname=$(basename "$1")
        printf %s/%s\\n "${_dir%%/}" "${_fname%%.*}"
    else
        printf %s\\n "${1%%.*}"
    fi
}

do_rm() {
    if [ "$CLEANER_DRYRUN" = "1" ]; then
        log "Would remove $1"
    else
        s3_rm "$1"
        [ "$S3_VERBOSE" = "1" ] && echo "$1"
    fi

    if [ -n "$CLEANER_SIBLINGS" ]; then
        for ext in $CLEANER_SIBLINGS; do
            sibling=$(no_extension "$1").${ext##.}
            if s3_exists "$sibling"; then
                log "Removing $sibling, sibling of $1 in $CLEANER_TARGET"
                if [ "$CLEANER_DRYRUN" = "1" ]; then
                    log "Would remove $sibling"
                else
                    s3_rm "$sibling"
                    [ "$S3_VERBOSE" = "1" ] && echo "$sibling"
                fi
            fi
        done
    fi
}

# Sanity check on arguments
[ -z "$CLEANER_TARGET" ] && abort "You need to specify a directory!"
[ -n "$CLEANER_SIBLINGS" ] && [ "$CLEANER_PATTERN" = '*' ] && abort "List of siblings is incompatible with match-all pattern"

# Convert human-period to seconds.
[ -n "$CLEANER_AGE" ] && CLEANER_AGE=$(howlong "$CLEANER_AGE")

s3_init
[ "$(s3_type "$CLEANER_TARGET")" != "folder" ] && abort "$CLEANER_TARGET must be a directory"

# Universally remove trailing slashes so we can assume there are not
CLEANER_TARGET=${CLEANER_TARGET%%/}

if [ -z "$CLEANER_AGE" ]; then
    if [ -n "${CLEANER_KEEP}" ] && [ "${CLEANER_KEEP}" -gt "0" ]; then
        log "*** $appname *** Keeping only ${CLEANER_KEEP} youngest file(s) matching $CLEANER_PATTERN in $CLEANER_TARGET"
        _files=$(s3_ls "${CLEANER_TARGET}/${CLEANER_PATTERN}")
        _nfiles=$(printf %s\\n "$_files" | wc -l)
        if [ "$_nfiles" -gt "$CLEANER_KEEP" ]; then
            for i in $(seq "$((CLEANER_KEEP + 1))" "$_nfiles"); do
                DELETE=$(printf %s\\n "$_files" | head -n "$i" | tail -n 1)
                log "Removing old copy $DELETE"
                do_rm "$DELETE"
            done
        fi
    else
        log "*** $appname *** Keeping all files matching $CLEANER_PATTERN in $CLEANER_TARGET"
    fi
else
    if [ -n "${CLEANER_KEEP}" ] && [ "${CLEANER_KEEP}" -gt "0" ]; then
        log "*** $appname *** Keeping only ${CLEANER_KEEP} youngest file(s) younger than $CLEANER_AGE secs. matching $CLEANER_PATTERN in $CLEANER_TARGET"
        _files=$(s3_ll "${CLEANER_TARGET}/${CLEANER_PATTERN}")
        _nfiles=$(printf %s\\n "$_files" | wc -l)
        if [ "$_nfiles" -gt "$CLEANER_KEEP" ]; then
            _now=$(date -u +'%s')
            _limit=$(( _now - CLEANER_AGE ))
            for i in $(seq "$((CLEANER_KEEP + 1))" "$_nfiles"); do
                IFS='	' read -r _mtime _size _path <<EOF
$(printf %s\\n "$_files" | head -n "$i" | tail -n 1)
EOF
                if [ -n "$_mtime" ] && [ "$_mtime" -lt "$_limit" ]; then
                    log "Removing old copy $_path"
                    do_rm "$_path"
                fi
            done
        fi
    else
        log "*** $appname *** Keeping file(s) younger than $CLEANER_AGE secs. matching $CLEANER_PATTERN in $CLEANER_TARGET"
        _files=$(s3_ll "${CLEANER_TARGET}/${CLEANER_PATTERN}")
        _nfiles=$(printf %s\\n "$_files" | wc -l)
        _now=$(date -u +'%s')
        _limit=$(( _now - CLEANER_AGE ))
        for i in $(seq "$((CLEANER_KEEP + 1))" "$_nfiles"); do
            # shellcheck disable=SC2034
            IFS='	' read -r _mtime _size _path <<EOF
$(printf %s\\n "$_files" | head -n "$i" | tail -n 1)
EOF
            if [ -n "$_mtime" ] && [ "$_mtime" -lt "$_limit" ]; then
                log "Removing old copy $_path"
                do_rm "$_path"
            fi
        done
    fi
fi

s3_cleanup

if [ $# -ne "0" ]; then
    log "Executing $*"
    exec "$@"
fi