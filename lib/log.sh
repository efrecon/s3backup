#!/usr/bin/env sh

S3_VERBOSE=${S3_VERBOSE:-0}
S3_TRACE=${S3_TRACE:-0}
if [ -t 1 ]; then
    INTERACTIVE=1
else
    INTERACTIVE=0
fi

# Colourisation support for logging and output.
_colour() {
  if [ "$INTERACTIVE" = "1" ]; then
    printf '\033[1;'${1}'m%b\033[0m' "$2"
  else
    printf -- "%b" "$2"
  fi
}
green() { _colour "32" "$1"; }
red() { _colour "31" "$1"; }
yellow() { _colour "33" "$1"; }
blue() { _colour "34" "$1"; }
purple() { _colour "35" "$1"; }
grey() { _colour "90" "$1"; }

# Conditional logging
log() {
  if [ "$S3_VERBOSE" = "1" ]; then
    echo "[$(blue "${2:-$appname}")] [$(yellow info)] [$(date +'%Y%m%d-%H%M%S')] $1" >&2
  fi
}

trace() {
  if [ "$S3_TRACE" = "1" ]; then
    echo "[$(blue "${2:-$appname}")] [$(grey trace)] [$(date +'%Y%m%d-%H%M%S')] $1" >&2
  fi
}

warn() {
  echo "[$(blue "${2:-$appname}")] [$(red WARN)] [$(date +'%Y%m%d-%H%M%S')] $1" >&2
}

abort() {
  warn "$1" "${2:-$appname}"
  exit 1
}
