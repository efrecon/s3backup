#!/usr/bin/env sh

# Performs glob matching, little like Tcl.
# $1 is the matching pattern
# $2 is the string to test against
__s3_glob() {
    # no quotes on purpose
    # shellcheck disable=2254
    case "$2" in
        $1) return 0;;
    esac
    return 1
}

MC_GLOBAL_OPTIONS=
MC_CONFIG_DIR=${HOME}/.mc
s3_options() {
    while [ $# -gt 0 ]; do
        case "$1" in
            -C | --config-dir)
                MC_CONFIG_DIR="$2"
                MC_GLOBAL_OPTIONS="$MC_GLOBAL_OPTIONS --config-dir $2"
                shift 2;;
            --config-dir=*)
                MC_CONFIG_DIR="${1#*=}"
                MC_GLOBAL_OPTIONS="$MC_GLOBAL_OPTIONS --config-dir ${1#*=}"
                shift;;
            --autocompletion | --quiet | -q | --no-color | --json | --debug | --insecure)
                MC_GLOBAL_OPTIONS="$MC_GLOBAL_OPTIONS $1"
                shift;;
            -*)
                abort "Unknown mc option!";;
        esac
    done
}

MC_ENCRYPT_KEY=${MC_ENCRYPT_KEY:-}
s3_encrypt() {
    MC_ENCRYPT_KEY="$1"
}

s3_init() {
    trace "Initialising s3 implementation on top of mc" mc
}

s3_cleanup() {
    trace "Cleaning up s3 implementation on top of mc" mc
}

# Wrapper around the regular mc command. The wrapper will automatically pass the
# global options that were registered to use using s3_options and will trigger
# encryption using the --encrypt-key for the relevant commands when s3_encrypt
# has been called or the environment variable MC_ENCRYPT_KEY set.
__s3_mc() {
    case "$1" in
        stat | cp | cat | mirror | head | pipe | du | rm)
            cmd=$1; shift
            if [ -z "$MC_ENCRYPT_KEY" ]; then
                # shellcheck disable=SC2086
                __mc $MC_GLOBAL_OPTIONS "$cmd" "$@"
            else
                # shellcheck disable=SC2086
                __mc $MC_GLOBAL_OPTIONS "$cmd" --enc-c "$MC_ENCRYPT_KEY" "$@"
            fi
            ;;
        *)
            cmd=$1; shift
            # shellcheck disable=SC2086
            __mc $MC_GLOBAL_OPTIONS "$cmd" "$@"
            ;;
    esac
}

__mc() {
    [ "$S3_TRACE" = "1" ] && log "Running: mc $*" mc
    # shellcheck disable=SC2086
    mc "$@"
}

s3_rm() {
    __s3_mc rm "$1"
}

s3_cp() {
    __s3_mc cp "$1" "$2"
}

s3_cat() {
    __s3_mc cat "$1"
}

# This is the same as: ls "$1" -1 -t
s3_ls() {
    if [ "$(s3_type "$1")" = "folder" ]; then
        __s3_mc ls --no-color "$1" |
            LC_ALL=C sort -r -k 1,2 |
            sed -E 's/\[.*\][[:space:]]*[0-9\.]+[[:alpha:]]*B[[:space:]]*//g'
    else
        _dir=$(dirname "$1")
        _ptn=$(basename "$1")
        # Iterate over the output of the ls on the directory, keep the literal
        # linefeed, where from the strange formatting for IFS (KEEP IT!!)
        s3_ls "$_dir" | while IFS='
' read -r fname; do
            if __s3_glob "$_ptn" "$fname"; then
                printf %s/%s\\n "$_dir" "$fname"
            fi
        done
    fi
}

__s3_to_bytes() {
    _sz=$(printf %s\\n "$1" | sed -E -e 's/([0-9\.]+)([[:alpha:]]*B)/\1/')
    _unit=$(printf %s\\n "$1" | sed -E -e 's/([0-9\.]+)([[:alpha:]]*B)/\2/')
    if printf %s\\n "$_unit" | grep -qi "kib"; then
        printf %d\\n "$(awk "BEGIN {print int($_sz * 1024)}")"
    elif printf %s\\n "$_unit" | grep -qi "kb"; then
        printf %d\\n "$(awk "BEGIN {print int($_sz * 1000)}")"
    elif printf %s\\n "$_unit" | grep -qi "mib"; then
        printf %d\\n "$(awk "BEGIN {print int($_sz * 1024 * 1024)}")"
    elif printf %s\\n "$_unit" | grep -qi "mb"; then
        printf %d\\n "$(awk "BEGIN {print int($_sz * 1000000)}")"
    elif printf %s\\n "$_unit" | grep -qi "gib"; then
        printf %d\\n "$(awk "BEGIN {print int($_sz * 1024 * 1024 * 1024)}")"
    elif printf %s\\n "$_unit" | grep -qi "gb"; then
        printf %d\\n "$(awk "BEGIN {print int($_sz * 1000000000)}")"
    elif printf %s\\n "$_unit" | grep -qi "tib"; then
        printf %d\\n "$(awk "BEGIN {print int($_sz * 1024 * 1024 * 1024 * 1024)}")"
    elif printf %s\\n "$_unit" | grep -qi "tb"; then
        printf %d\\n "$(awk "BEGIN {print int($_sz * 1000000000000)}")"
    elif printf %s\\n "$_unit" | grep -qi "b"; then
        printf %d\\n "$_sz"
    fi
}

s3_ll() {
    if [ "$(s3_type "$1")" = "folder" ]; then
        while IFS= read -r line || [ -n "$line" ]; do
            _mtime=$(printf %s\\n "$line" | sed -E -e 's/^\[([0-9]{4}-[0-9]{2}-[0-9]{2}[[:space:]][0-9]{2}:[0-9]{2}:[0-9]{2})[[:space:]]UTC\][[:space:]]+([0-9\.]+[[:alpha:]]*B)[[:space:]]+(.*)/\1/g')
            _sz=$(printf %s\\n "$line" | sed -E -e 's/^\[([0-9]{4}-[0-9]{2}-[0-9]{2}[[:space:]][0-9]{2}:[0-9]{2}:[0-9]{2})[[:space:]]UTC\][[:space:]]+([0-9\.]+[[:alpha:]]*B)[[:space:]]+(.*)/\2/g')
            _path=$(printf %s\\n "$line" | sed -E -e 's/^\[([0-9]{4}-[0-9]{2}-[0-9]{2}[[:space:]][0-9]{2}:[0-9]{2}:[0-9]{2})[[:space:]]UTC\][[:space:]]+([0-9\.]+[[:alpha:]]*B)[[:space:]]+(.*)/\3/g')
            printf %d\\t%d\\t%s\\n \
                "$(date -u -d "$_mtime" +'%s')" \
                "$(__s3_to_bytes "$_sz")" \
                "${1%%/}/$_path"
        done <<EOF
$(__s3_mc ls --no-color "${1%%/}/" | LC_ALL=C sort -r -k 1,2)
EOF
    else
        _dir=$(dirname "$1")
        _ptn=$(basename "$1")
        # Iterate over the output of the ls on the directory, keep the literal
        # linefeed, where from the strange formatting for IFS (KEEP IT!!)
        s3_ll "$_dir" | while IFS='	' read -r _mtime _sz _path; do
            if __s3_glob "$_ptn" "$(basename "$_path")"; then
                printf %d\\t%d\\t%s/%s\\n \
                    "$_mtime" \
                    "$_sz" \
                    "$_dir" \
                    "$(basename "$_path")"
            fi
        done
    fi
}

# This is the same as: stat -c "%s" "$1"
s3_size() {
    __s3_mc stat --no-color --json "$1" |
        grep "size" |
        sed -E -e 's/.*"size"\s*:\s*([0-9]+).*/\1/g'
}

s3_mtime() {
    mdate=$(    __s3_mc ls --no-color "$1" |
                sed -E 's/^\[([0-9]{4}-[0-9]{2}-[0-9]{2}[[:space:]][0-9]{2}:[0-9]{2}:[0-9]{2})[[:space:]]UTC\].*/\1/g' |
                LC_ALL=C sort -r -k 1,2 |
                head -n 1)
    [ -n "$mdate" ] && date -u -d "$mdate" +'%s'
}

# This is the same as test -f "$1"
s3_exists() {
    __s3_mc stat --no-color "$1" > /dev/null 2>&1
}

# Returns false when files differ. When they are of different sizes, diff has to
# be called to check content.
s3_diff() {
    # Get sizes of files
    sz_1=$(s3_size "$1")
    sz_2=$(s3_size "$2")

    if [ "$sz_1" = "$sz_2" ]; then
        # When both files have the same size, we need to peek inside. Copy one
        # of the files to a temporary location, compare the copy against the
        # content of the second (diff cannot use two streams) and test
        val_1=$(mktemp)
        s3_cp "$1" "$val_1"
        df=$(s3_cat "$2" | diff -q "$val_1" -)
        rm -f "$val_1"
        test -z "$df"
    else
        return 1
    fi

}

# Returns the type of the argument, in a mc compatible way for all cases (incl.
# local filesystem). This will be one of file or folder.
s3_type() {
    _loc=$(s3_detect "$1")
    _type=
    case "$_loc" in
        s3)
            _type=$( __s3_mc stat --no-color "$1" 2>/dev/null |
                     grep -E "^[Tt]ype" |
                     sed -E -e 's/^[Tt]ype\s*:\s*([[:alnum:]]+).*/\1/g' )
            [ -z "$_type" ] && return 1
            ;;
        local)
            # mc only has two recognised types: file and folder.
            # shellcheck disable=2209
            case "$(stat -L -c "%F" "$1" 2>/dev/null)" in
                dir*) _type=folder;;
                *file) _type=file;;
                *) return 1;;
            esac
            ;;
        *)
            warn "$_loc not a recognised provider";
            return 1;;
    esac
    unset _loc
    printf %s\\n "$_type"
}

# Return local or s3, depending on where object passed as a parameter is
# located. This supposes a standard location for the mc configuration file as we
# need to peek inside.
s3_detect() {
    if printf %s\\n "$1" | grep -qE '^(/|\.)'; then
        echo "local"
    else
        root=$(printf %s\\n "$1" | cut -d '/' -f 1)
        # Look for the name of the root folder in the JSON configuration
        # file, forcing a JSON construct around. This isn't foolproof, but
        # close by as we know that the root is a folder.
        if [ -f "${MC_CONFIG_DIR%%/}/config.json" ] && grep -qE "\"$root\"\s*:\s*\{" "${MC_CONFIG_DIR%%/}/config.json"; then
            echo "s3"
        else
            echo "local"
        fi
    fi
}

s3_resolve() {
    # XXX : Fixme
    echo "NYI"
}

# This is the same as mkdir -p
s3_mkdir() {
    __s3_mc mb "$@"
}