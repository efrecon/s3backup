#!/usr/bin/env sh

######
# IMPORTANT: It is up to the CALLER script to load the s3_${S3}.sh
# implementation ONCE this module has been loaded.
######

# This variable picks the underlying implementation to use. It should be one of
# mc or s3cmd. The default is to choose S3 implementation based on what is
# available under the path, preference to mc.
S3=${S3:-}

# When empty, choose S3 command for operating on buckets. Prefer mc over s3cmd
# for now since it has been battle tested.
if [ -z "$S3" ]; then
  if command -v mc >/dev/null; then
    S3=mc
  elif command -v s3cmd >/dev/null; then
    S3=s3cmd
  fi
fi

# We must have an implementation, and a recognised one to operate on buckets!
if [ -n "$S3" ]; then
  case "$S3" in
    mc | s3cmd)
      log "Performing S3 calls with $S3" s3;;
    default)
      abort "$S3 is not a recognised implementation for working with S3 buckets!" s3;;
  esac
else
  abort "One of s3cmd or mc is needed for operating on remote S3 buckets!" s3
fi

# All implementations should implement the following functions.
s3_init() { false; }
s3_options() { false; }
s3_encrypt() { false; }
s3_cleanup() { false; }
s3_rm() { false; }
s3_cp() { false; }
s3_cat() { false; }
s3_ls() { false; }
s3_ll() { false; }
s3_size() { false; }
s3_mtime() { false; }
s3_exists() { false; }
s3_diff() { false; }
s3_type() { false; }
s3_detect() { false; }
s3_resolve() { false; }
s3_mkdir() { false; }
