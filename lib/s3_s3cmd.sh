#!/usr/bin/env sh

# Name of the empty file that will be created to ensure that directories created
# using s3_mkdir actually exist.
S3CMD_EMPTY=${S3CMD_EMPTY:-.s3empty}

# Path to the configuration directory where alias files are looked up, and where
# relative s3cmd alias configuration files are looked up.
S3CMD_CONFIG_DIR=${S3CMD_CONFIG_DIR:-${HOME}/.s3cmd}

# Path to the file containing aliases indirections, when relative it is looked
# up in the configuration directory.
S3CMD_ALIASES=${S3CMD_ALIASES:-aliases}

# Extension that are automatically added to the name of the "perhaps" alias when
# auto-resolution is on or when no path is provided in the aliases indirection
# file.
S3CMD_CONFIG_EXT=${S3CMD_CONFIG_EXT:-cfg}

# Automatically resolve relative paths looking like <alias>/xx. When such a path
# is given, <alias> followed by the .cfg extension, will be looked up in the
# configuration directory.
S3CMD_AUTORESOLVE=${S3CMD_AUTORESOLVE:-1}

# What should we cache for quicker operations. For the time being: 'alias' will
# cache aliases resolution for the time of a session. 'folder' will cache which
# path are folders as discovery requires several round trips.
S3CMD_CACHE="alias folder"


S3CMD_GLOBAL_OPTIONS=; # Global mc-compatible options.
S3CMD_FLAGS=;          # Double-dashed flags recognised by s3cmd
S3CMD_OPTIONS=;        # Double-dashed options recognised by s3cmd
S3CMD_CACHE_ALIASES=;  # Path to alias cache
S3CMD_CACHE_FOLDER=;   # Path to folder cache
S3CMD_CACHE_FILE=;     # Path to file cache, NYI
s3_options() {
    while [ $# -gt 0 ]; do
        case "$1" in
            --aliases)
                S3CMD_ALIASES="$2"; shift 2;;
            --aliases=*)
                S3CMD_ALIASES="${1#*=}"; shift;;

            --no-autoresolve)
                S3CMD_AUTORESOLVE=0; shift;;

            -C | --config-dir)
                S3CMD_CONFIG_DIR="$2"; shift 2;;
            --config-dir=*)
                S3CMD_CONFIG_DIR="${1#*=}"; shift;;

            --autocompletion | --no-color | --json)
                warn "$1 not supported on top of s3cmd"; shift;;

            --quiet | -q | --debug)
                S3CMD_GLOBAL_OPTIONS="$S3CMD_GLOBAL_OPTIONS $1"
                shift;;

            --insecure)
                S3CMD_GLOBAL_OPTIONS="$S3CMD_GLOBAL_OPTIONS --no-check-certificate --no-check-hostname"
                shift;;
            -*)
                abort "Unknown option!";;
        esac
    done
}

# Performs glob matching, little like Tcl.
# $1 is the matching pattern
# $2 is the string to test against
__s3_glob() {
    # no quotes on purpose
    # shellcheck disable=2254
    case "$2" in
        $1) return 0;;
    esac
    return 1
}


# Initialise this module. You MUST call this first if you want anything to work
# properly!
s3_init() {
    # Discover and cache known double-dashed options to the s3cmd. This builds
    # upon an analysis of the --help output of s3cmd. Options have to be
    # followed by an equal sign to be considered such.
    trace "Initialising s3 implementation on top of s3cmd" s3cmd
    if [ -z "$S3CMD_OPTIONS" ]; then
        log "Collecting known options of s3cmd" s3cmd
        S3CMD_OPTIONS=$(    s3cmd --help |
                            grep -E \
                                -e '^[[:space:]]{2}--[a-z_-]+=' \
                                -e '^[[:space:]]{2}-[a-z].*--[a-z_-]+=' |
                            sed -E -e 's/.*(--[a-z_-]+)=.*/\1/g' )
    fi

    # Discover and cache known double-dashed flags to the s3cmd. This builds
    # upon an analysis of the --help output of s3cmd. Flags have to be
    # followed by a space to be considered such.
    if [ -z "$S3CMD_FLAGS" ]; then
        log "Collecting known flags of s3cmd" s3cmd
        S3CMD_FLAGS=$(    s3cmd --help |
                            grep -E \
                                -e '^[[:space:]]{2}--[a-z_-]+[[:space:]]' \
                                -e '^[[:space:]]{2}-[a-z].*--[a-z_-]+[[:space:]]' |
                            sed -E -e 's/.*(--[a-z_-]+)[[:space:]].*/\1/g' )
    fi

    # Create caches in temp space depending on the value of S3CMD_CACHE
    if [ -z "$S3CMD_CACHE_FOLDER" ] && printf %s\\n "$S3CMD_CACHE" | grep -q "folder"; then
        S3CMD_CACHE_FOLDER=$(mktemp)
    fi
    if [ -z "$S3CMD_CACHE_FILE" ] && printf %s\\n "$S3CMD_CACHE" | grep -q "file"; then
        S3CMD_CACHE_FILE=$(mktemp)
    fi
    if [ -z "$S3CMD_CACHE_ALIASES" ] && printf %s\\n "$S3CMD_CACHE" | grep -q "alias"; then
        S3CMD_CACHE_ALIASES=$(mktemp)
    fi
}

# Cleanup this module. You should call this when your program ends.
s3_cleanup() {
    # Remove all caches.
    trace "Cleaning up s3 implementation on top of s3cmd" s3cmd
    if [ -n "$S3CMD_CACHE_FOLDER" ] && [ -f "$S3CMD_CACHE_FOLDER" ]; then
        rm -f "$S3CMD_CACHE_FOLDER"
    fi
    if [ -n "$S3CMD_CACHE_FILE" ] && [ -f "$S3CMD_CACHE_FILE" ]; then
        rm -f "$S3CMD_CACHE_FILE"
    fi
    if [ -n "$S3CMD_CACHE_ALIASES" ] && [ -f "$S3CMD_CACHE_ALIASES" ]; then
        rm -f "$S3CMD_CACHE_ALIASES"
    fi
}

s3_encrypt() {
    warn "Not supported!"
}

# Wrapper around the regular s3cmd command. The wrapper will automatically pass
# the global options that were registered to use using s3_options. In addition,
# any double-dash option or flag that is supported by s3cmd can be passed along.
# These have to be recognised by us, which we dynamically discover as part of
# the s3_init() function.
__s3_s3cmd() {
    __local_opts=
    while [ $# -gt 0 ]; do
        case "$1" in
            --autocompletion | --no-color | --json)
                warn "$1 not supported on top of s3cmd"; shift;;
            --)
                shift; break;;
            --*)
                _val=${1#*=};  # The value, when present is everything after the =
                _key=${1%=*};  # This is everything before the =, when present
                if [ "$_key" = "$_val" ]; then
                    # This captures options that look like --something (no equal
                    # sign)
                    if printf %s\\n "$S3CMD_OPTIONS" | grep -q -e "$_key"; then
                        __local_opts="$__local_opts ${_key}=$2"; shift 2
                    elif printf %s\\n "$S3CMD_FLAGS" | grep -q -e "$_key"; then
                        __local_opts="$__local_opts ${_key}"; shift
                    else
                        warn "$_key is neither a double-dashed flag nor an option to s3cmd" s3cmd
                    fi
                else
                    if printf %s\\n "$S3CMD_OPTIONS" | grep -q -e "$_key"; then
                        __local_opts="$__local_opts ${_key}=${_val}"; shift
                    else
                        warn "$_key is not a double-dashed option to s3cmd" s3cmd
                    fi
                fi
                ;;
            -*)
                warn "Unknown option/flag: $1! Note: only double-dashed options/flags supported" s3cmd;;
            *)
                break;;
        esac
    done

    cmd=$1; shift
    trace "Running: s3cmd $S3CMD_GLOBAL_OPTIONS $__local_opts $cmd $*"
    # shellcheck disable=SC2086
    s3cmd $S3CMD_GLOBAL_OPTIONS $__local_opts "$cmd" "$@" 2>/dev/null
}


s3_rm() {
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
    case "$_provider" in
        s3)
            # shellcheck disable=SC2086
            __s3_s3cmd --config "$_cfg" rm "$_path"
            ;;
        local)
            rm "$_path"
            ;;
        *)
            warn "$_provider not a recognised provider" s3cmd
            return 1;;
    esac
}

s3_cp() {
    _local_flags=
    _s3cmd_flags=

    while [ $# -gt 0 ]; do
        case "$1" in
            -f | --force)
                _local_flags="${_local_flags} -f"
                _s3cmd_flags="${_s3cmd_flags} --force"
                shift;;
            --)
                shift; break;;
            -*)
                warn "Unknown flag: $1 !" s3cmd; shift;;
            *)
                break;;
        esac
    done

    [ $# -lt 2 ] && return 1
        # shellcheck disable=SC2034
        IFS='	' read -r _provider_src _path_src _alias_src _cfg_src<<EOF
$(__s3_resolve "$1")
EOF
        # shellcheck disable=SC2034
        IFS='	' read -r _provider_dst _path_dst _alias_dst _cfg_dst <<EOF
$(__s3_resolve "$2")
EOF
    if [ "$_provider_src" = "local" ] && [ "$_provider_dst" = "local" ]; then
        # shellcheck disable=SC2086
        cp $_local_flags "$_path_src" "$_path_dst"
    elif [ "$_provider_src" = "local" ] && [ "$_provider_dst" = "s3" ]; then
        # shellcheck disable=SC2086
        __s3_s3cmd --config "$_cfg_dst" $_s3cmd_flags put "$_path_src" "$_path_dst"
    elif [ "$_provider_src" = "s3" ] && [ "$_provider_dst" = "local" ]; then
        # shellcheck disable=SC2086
        __s3_s3cmd --config "$_cfg_src" $_s3cmd_flags get "$_path_src" "$_path_dst"
    elif [ "$_provider_src" = "s3" ] && [ "$_provider_dst" = "local" ]; then
        _tmp=$(mktemp)
        # Always force as the temporary file is created by mktemp
        __s3_s3cmd --config "$_cfg_src" --force get "$_path_src" "$_tmp"
        # shellcheck disable=SC2086
        __s3_s3cmd --config "$_cfg_dst" $_s3cmd_flags put "$_tmp" "$_path_dst"
        rm -f "$_tmp"
    fi
}

s3_cat() {
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
    case "$_provider" in
        s3)
            # shellcheck disable=SC2086
            __s3_s3cmd --config "$_cfg" --quiet get "$_path" -
            ;;
        local)
            cat "$_path"
            ;;
        *)
            warn "$_provider not a recognised provider" s3cmd
            return 1;;
    esac
}

__s3_ll() {
    case "$1" in
        s3)
            while IFS= read -r line || [ -n "$line" ]; do
                if printf %s\\n "$line" | grep -qE '^[[:space:]]+DIR'; then
                    _mtime=0
                    _sz=0
                    _path=$(printf %s\\n "$line" | sed -E 's/^[[:space:]]+DIR[[:space:]]+(.*)/\1/')
                else
                    _mdate=$(printf %s\\n "$line" | sed -E 's/^([0-9]{4}-[0-9]{2}-[0-9]{2}[[:space:]][0-9]{2}:[0-9]{2})[[:space:]]+([0-9]+)[[:space:]]+(.*)/\1/')
                    _mtime=$(date -u -d "$_mdate" +'%s')
                    _sz=$(printf %s\\n "$line" | sed -E 's/^([0-9]{4}-[0-9]{2}-[0-9]{2}[[:space:]][0-9]{2}:[0-9]{2})[[:space:]]+([0-9]+)[[:space:]]+(.*)/\2/')
                    _path=$(printf %s\\n "$line" | sed -E 's/^([0-9]{4}-[0-9]{2}-[0-9]{2}[[:space:]][0-9]{2}:[0-9]{2})[[:space:]]+([0-9]+)[[:space:]]+(.*)/\3/')
                fi

                if [ -n "$4" ]; then
                    printf %d\\t%d\\t%s\\n "$_mtime" "$_sz" "$(printf %s\\n "$_path" | sed -E -e "s~^s3:/~${4}~g")"
                else
                    printf %d\\t%d\\t%s\\n "$_mtime" "$_sz" "$_path"
                fi
            done <<EOF
$(__s3_s3cmd --config "$3" ls "$2" | grep -v -E -e "/${S3CMD_EMPTY}\$" | LC_ALL=C sort -r -k 1,2)
EOF
            ;;
        local)
            # shellcheck disable=SC2012
            ls "$2" -1 -t | while IFS='
' read -r fname; do
                _fpath=$fname
                [ -d "$2" ] && _fpath=$(printf %s/%s\\n "${2%%/}" "${fname}")
                _mtime=$(stat -c "%Y" "$_fpath")
                _sz=$(stat -c "%s" "$_fpath")
                printf %d\\t%d\\t%s\\n "$_mtime" "$_sz" "$_fpath"
            done
            ;;
        *)
            warn "$1 not a recognised provider" s3cmd
            return 1;;
    esac
}

s3_ll() {
    _ftype=$(s3_type "$1" || true)
    if [ "$_ftype" = "folder" ]; then
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
        __s3_ll "$_provider" "${_path%%/}/" "$_cfg" "$_alias"
    elif [ "$_ftype" = "file" ]; then
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
        __s3_ll "$_provider" "$_path" "$_cfg" "$_alias"
    else
        _dir=$(dirname "$1")
        _ptn=$(basename "$1")
        # Iterate over the output of the ls on the directory, keep the literal
        # linefeed, where from the strange formatting for IFS (KEEP IT!!)
        s3_ll "$_dir" | while IFS='	' read -r mtime sz fpath; do
            if __s3_glob "$_ptn" "$(basename "$fpath")"; then
                printf %d\\t%d\\t%s\\n "$mtime" "$sz" "$fpath"
            fi
        done
    fi
}

__s3_ls() {
    case "$1" in
        s3)
            if [ -n "$4" ]; then
                # shellcheck disable=SC2086
                __s3_s3cmd --config "$3" ls "$2" |
                    LC_ALL=C sort -r -k 1,2 |
                    cut -c32- |
                    grep -v -E "/${S3CMD_EMPTY}\$" |
                    sed -E -e "s~^s3:/~${4}~g"
            else
                # shellcheck disable=SC2086
                __s3_s3cmd --config "$3" ls "$2" |
                    LC_ALL=C sort -r -k 1,2 |
                    cut -c32- |
                    grep -v -E "/${S3CMD_EMPTY}\$"
            fi
            ;;
        local)
            if [ -d "$2" ]; then
                # shellcheck disable=SC2012
                ls "$2" -1 -t | while IFS='
' read -r fname; do
                    printf %s/%s\\n "${2%%/}" "${fname}"
                done
            else
                ls "$2" -1 -t
            fi
            ;;
        *)
            warn "$1 not a recognised provider" s3cmd
            return 1;;
    esac
}

# This is the same as: ls "$1" -1 -t
s3_ls() {
    _ftype=$(s3_type "$1" || true)
    if [ "$_ftype" = "folder" ]; then
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
        __s3_ls "$_provider" "${_path%%/}/" "$_cfg" "$_alias"
    elif [ "$_ftype" = "file" ]; then
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
        __s3_ls "$_provider" "$_path" "$_cfg" "$_alias"
    else
        _dir=$(dirname "$1")
        _ptn=$(basename "$1")
        # Iterate over the output of the ls on the directory, keep the literal
        # linefeed, where from the strange formatting for IFS (KEEP IT!!)
        s3_ls "$_dir" | while IFS='
' read -r fpath; do
            if __s3_glob "$_ptn" "$(basename "$fpath")"; then
                printf %s\\n "$fpath"
            fi
        done
    fi
}

# This is the same as: stat -c "%s" "$1"
s3_size() {
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
    case "$_provider" in
        s3)
            __s3_s3cmd --config "$_cfg" info "$_path" |
                grep -Ei 'file size:' |
                sed -E -e 's/[[:space:]]*[Ff]ile [Ss]ize:[[:space:]]*([0-9]+).*/\1/'
            ;;
        local)
            stat -c "%s" "$_path"
            ;;
        *)
            warn "$_provider not a recognised provider" s3cmd
            return 1;;
    esac
}

# This is the same as: stat -c "%Y" "$1", i.e. mtime in seconds since epoch.
# Only works for directories with a / at then on top of s3.
s3_mtime() {
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
    case "$_provider" in
        s3)
            mdate=$(__s3_s3cmd --config "$_cfg" ls "$_path" |
                    grep -vE '^[[:space:]]+DIR' |
                    sed -E 's/^([0-9]{4}-[0-9]{2}-[0-9]{2}[[:space:]][0-9]{2}:[0-9]{2})[[:space:]]+.*/\1/g' |
                    LC_ALL=C sort -r -k 1,2 |
                    head -n 1)
            [ -n "$mdate" ] && date -u -d "$mdate" +'%s'
            ;;
        local)
            stat -c "%Y" "$_path"
            ;;
        *)
            warn "$_provider not a recognised provider" s3cmd
            return 1;;
    esac
}

# This is the same as test -f "$1"
s3_exists() {
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
    case "$_provider" in
        s3)
            _fsize=$(__s3_s3cmd --config "$_cfg" info "$_path" | grep -Ei 'file size:')
            if [ -z "$_fsize" ]; then
                return 1
            fi
            ;;
        local)
            test -f "$_path"
            ;;
        *)
            warn "$_provider not a recognised provider" s3cmd
            return 1;;
    esac
}

# Returns false when files differ. When they are of different sizes, diff has to
# be called to check content.
s3_diff() {
    # Get sizes of files
    sz_1=$(s3_size "$1")
    sz_2=$(s3_size "$2")

    if [ "$sz_1" = "$sz_2" ]; then
        # When both files have the same size, we need to peek inside. We try to
        # avoid fetching from remote as much as possible and work on local files
        # whenever possible.
        IFS='	' read -r _provider_1 _path_1 _alias_1 _cfg_1<<EOF
$(__s3_resolve "$1")
EOF
        # shellcheck disable=SC2034
        IFS='	' read -r _provider_2 _path_2 _alias_2 _cfg_2 <<EOF
$(__s3_resolve "$2")
EOF

        if [ "$_provider_1" = "local" ] && [ "$_provider_2" = "local" ]; then
            diff -q "$_path_1" "$_path_2"
        elif [ "$_provider_1" = "local" ] && [ "$_provider_2" = "s3" ]; then
            df=$(s3_cat "$2" | diff -q "$_path_1" -)
            test -z "$df"
        elif [ "$_provider_1" = "s3" ] && [ "$_provider_2" = "local" ]; then
            df=$(s3_cat "$1" | diff -q "$_path_2" -)
            test -z "$df"
        elif [ "$_provider_1" = "s3" ] && [ "$_provider_2" = "s3" ]; then
            # Copy one of the files to a temporary location, compare the copy
            # against the content of the second (diff cannot use two streams)
            # and test
            val_1=$(mktemp)
            s3_cp -f -- "$1" "$val_1"
            df=$(s3_cat "$2" | diff -q "$val_1" -)
            rm -f "$val_1"
            test -z "$df"
        fi
    else
        return 1
    fi

}

# Returns the type of the argument, in a mc compatible way for all cases (incl.
# local filesystem). This will be one of file or folder.
s3_type() {
    if [ -n "$S3CMD_CACHE_FOLDER" ] && grep -q "${1%%/}/" "$S3CMD_CACHE_FOLDER"; then
        _type=folder
    else
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
        case "$_provider" in
            s3)
                if printf %s\\n "$_path" | grep -qE 's3://([a-z0-9.-]+)/$'; then
                    _type=folder
                    [ -n "$S3CMD_CACHE_FOLDER" ] && printf %s\\n "${1%%/}/" >> "$S3CMD_CACHE_FOLDER"
                elif printf %s\\n "$_path" | grep -qE '/$' && \
                    __s3_s3cmd --config "$_cfg" info "$_path" | grep -qEi 'File size: 0'; then
                    # shellcheck disable=2209
                    _type=folder
                    [ -n "$S3CMD_CACHE_FOLDER" ] && printf %s\\n "${1%%/}/" >> "$S3CMD_CACHE_FOLDER"
                elif printf %s\\n "$_path" | grep -qE '/$' && \
                    __s3_s3cmd --config "$_cfg" info "${_path%%/}/${S3CMD_EMPTY}" | grep -qEi 'File size: 0'; then
                    # shellcheck disable=2209
                    _type=folder
                    [ -n "$S3CMD_CACHE_FOLDER" ] && printf %s\\n "${1%%/}/" >> "$S3CMD_CACHE_FOLDER"
                elif __s3_s3cmd --config "$_cfg" info "$_path" | grep -qEi 'File size: [1-9]'; then
                    # shellcheck disable=2209
                    _type=file
                    [ -n "$S3CMD_CACHE_FILE" ] && printf %s\\n "$1" >> "$S3CMD_CACHE_FILE"
                elif __s3_s3cmd --config "$_cfg" ls "$(dirname "$_path")/" | grep DIR | grep -q -e "$_path"; then
                    # shellcheck disable=2209
                    _type=folder
                    [ -n "$S3CMD_CACHE_FOLDER" ] && printf %s\\n "${1%%/}/" >> "$S3CMD_CACHE_FOLDER"
                else
                    return 1
                fi
                ;;
            local)
                # mc only has two recognised types: file and folder.
                # shellcheck disable=2209
                case "$(stat -L -c "%F" "$1" 2>/dev/null)" in
                    dir*) _type=folder;;
                    *file) _type=file;;
                    *) return 1;;
                esac
                ;;
            *)
                warn "$_provider not a recognised provider" s3cmd
                return 1;;
        esac
    fi
    printf %s\\n "$_type"
}

__s3_abs_path() {
    printf %s\\n "$1" | grep -qE -e '^/' -e '^~' -e '^./' -e '^../'
}

__s3_bucket() {
    printf %s\\n "$1" | sed -E -e 's~s3://([a-z0-9.-]+).*$~\1~'
}

__s3_config_path() {
    # Read path to config, pick a good default out of the name
    # of the alias itself.
    __cfg=$2
    if [ -z "$__cfg" ]; then
        __cfg="${1}.${S3CMD_CONFIG_EXT##.}"
    fi

    # Resolve the configuration path when it is not absolute. When no container
    # directory is specified, we resolve using the main configuration directory.
    if __s3_abs_path "$__cfg"; then
        __cfg_path=$__cfg
    else
        if [ -z "$3" ]; then
            __cfg_path=${S3CMD_CONFIG_DIR%%/}/$__cfg
        else
            __cfg_path=${3%%/}/$__cfg
        fi
    fi

    # Log/warn when we have a file, and break out of the loop
    if [ -f "$__cfg_path" ]; then
        log "s3cmd configuration for $1 is at $__cfg_path" s3cmd
        printf %s\\n "$__cfg_path"
    else
        warn "Missing s3cmd configuration for $1 at $__cfg_path!" s3cmd
        printf \\n
    fi
}

__s3_resolve_config() {
    __cfg_path=
    if printf %s\\n "$S3CMD_CACHE" | grep -q alias; then
        __cfg_path=$(awk -F '\t' "\$1 ~ /$1/ {print \$2}" < "$S3CMD_CACHE_ALIASES")
    fi

    if [ -z "$__cfg_path" ]; then
        # Find location of aliases file, aliases will point a leading token, e.g.
        # gcs, to a s3cmd configuration file.
        __aliases_path=
        if __s3_abs_path "$S3CMD_ALIASES"; then
            __aliases_path=$S3CMD_ALIASES
        else
            __aliases_path=${S3CMD_CONFIG_DIR%%/}/$S3CMD_ALIASES
        fi

        # Resolve __cfg_path so that it will contain the full path to the
        # configuration file for the token that was passed as a parameter. This
        # parses the aliases file, looking for the path to the s3cmd configuration
        # file. By default, we look for a file with the same name as the token,
        # followed by the extension .cfg in the configuration directory.
        if [ -f "$__aliases_path" ]; then
            # shellcheck disable=SC2094
            while IFS= read -r line || [ -n "$line" ]; do
                line=$(printf %s\\n "$line" | sed '/^[[:space:]]*$/d' | sed '/^[[:space:]]*#/d')
                if [ -n "$line" ]; then
                    __alias=$(printf %s\\n "$line" | awk '{print $1}')
                    if [ "$__alias" = "$1" ]; then
                        # shellcheck disable=SC2094
                        __cfg_path=$(__s3_config_path \
                                        "$__alias" \
                                        "$(printf %s\\n "$line" | awk '{print $2}')" \
                                        "$(dirname "$__aliases_path")")
                        break
                    fi
                fi
            done <"$__aliases_path"
        fi

        # When the token wasn't found explicitely in the aliases file, we try just
        # with its name when auto resolution is on.
        if [ -z "$__cfg_path" ] && [ "$S3CMD_AUTORESOLVE" = "1" ]; then
            __cfg_path=$(__s3_config_path "$1")
        fi

        if printf "%s\n" "$S3CMD_CACHE" | grep -q alias; then
            printf %s\\t%s\\n "$1" "$__cfg_path" >> "$S3CMD_CACHE_ALIASES"
        fi
    fi
    printf %s\\n "$__cfg_path"
}


# Return local or s3, depending on where object passed as a parameter is
# located. This supposes a standard location for the mc configuration file as we
# need to peek inside.
__s3_config() {
    if __s3_abs_path "$1"; then
        printf %s\\t%s\\t%s\\n "local" "" ""
    else
        root=$(printf %s\\n "$1" | cut -d '/' -f 1)
        if [ "$root" = "s3:" ]; then
            printf %s\\n "s3"
        else
            _cfg=$(__s3_resolve_config "$root")
            if [ -n "$_cfg" ]; then
                printf %s\\t%s\\t%s\\n "s3" "$root" "$_cfg"
            else
                printf %s\\t%s\\t%s\\n "local" "" ""
            fi
        fi
    fi
}

s3_detect() {
    __s3_config "$1" | awk -F '\t' '{print $1}'
}

__s3_resolve() {
        IFS='	' read -r _loc _alias _cfg <<EOF
$(__s3_config "$1")
EOF
    case "$_loc" in
        local)
            printf %s\\t%s\\t%s\\t%s\\n "$_loc" "$1" "$_alias" "$_cfg";;
        s3)
            if printf %s\\n "$1" | grep -qEi '^s3://'; then
                printf %s\\t%s\\t%s\\t%s\\n "$_loc" "$1" "$_alias" "$_cfg"
            else
                _url=$(printf %s\\n "$1" | sed -E -e 's~^[^/]+/~s3://~' -e 's~^[^/]+$~s3://~')
                printf %s\\t%s\\t%s\\t%s\\n "$_loc" "$_url" "$_alias" "$_cfg"
            fi
            ;;
    esac
}

s3_resolve() {
    __s3_resolve "$1" | awk -F '\t' '{print $4}'
}

# This is the same as mkdir -p. On S3, it will create an empty file called
# $S3CMD_EMPTY under the directory.
s3_mkdir() {
    empty=$(mktemp)
    for d in "$@"; do
        IFS='	' read -r _provider _path _alias _cfg <<EOF
$(__s3_resolve "$1")
EOF
        case "$_provider" in
            s3)
                _bucket=$(__s3_bucket "$_path")
                if ! __s3_s3cmd --config "$_cfg" --quiet info "s3://${_bucket}/"; then
                    log "Creating bucket $_bucket" s3cmd
                    __s3_s3cmd --config "$_cfg" --quiet mb "s3://${_bucket}/"
                    __s3_s3cmd --config "$_cfg" --quiet put "$empty" "${_path%%/}/${S3CMD_EMPTY}"
                elif ! __s3_s3cmd --config "$_cfg" --quiet info "s3://${_bucket}/${S3CMD_EMPTY}"; then
                    __s3_s3cmd --config "$_cfg" --quiet put "$empty" "${_path%%/}/${S3CMD_EMPTY}"
                fi
                ;;
            local)
                mkdir -p "$d"
                ;;
            *)
                warn "$_provider not a recognised provider" s3cmd
                return 1;;
        esac
    done
    rm -f "$empty"
}
